﻿namespace OperacoesUrbanas.Enums
{

    public enum TipoParametroEnum : int
    {
        OPERADOR=1,
        VARIAVEL=2,
        CONSTANTE=3,
        BOOLEAN=4,
        LEITURA=5
    }

}

