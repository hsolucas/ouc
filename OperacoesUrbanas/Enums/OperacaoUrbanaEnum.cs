﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OperacoesUrbanas.Enums
{
    public enum OperacaoUrbanaEnum : int
    {
        AGUA_ESPRAIADA = 1,
        FARIA_LIMA = 2
    }
}