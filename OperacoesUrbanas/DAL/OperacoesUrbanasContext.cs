namespace OperacoesUrbanas.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class OperacoesUrbanasContext : DbContext
    {
        public OperacoesUrbanasContext()
            : base("name=OperacoesUrbanasContext")
        {
        }

        public virtual DbSet<CategoriaUso> categoriasUso { get; set; }
        public virtual DbSet<ClassificacaoPessoa> classificacoesPessoa { get; set; }
        public virtual DbSet<Imovel> imoveis { get; set; }
        public virtual DbSet<ImovelProposta> imoveisProposta { get; set; }
        public virtual DbSet<InteressadoProposta> interessadosProposta { get; set; }
        public virtual DbSet<ProprietarioCepacProposta> proprietariosCepacProposta { get; set; }
        public virtual DbSet<ContatoProposta> contatosProposta { get; set; }
        public virtual DbSet<OperacaoUrbana> operacoesUrbanas { get; set; }
        public virtual DbSet<ParametroCalculoCepac> parametrosCalculoCepac { get; set; }
        public virtual DbSet<ParametroOperacaoUrbana> parametrosOperacaoUrbana { get; set; }
        public virtual DbSet<Pessoa> pessoas { get; set; }
        public virtual DbSet<Proposta> propostas { get; set; }
        public virtual DbSet<Setor> setores { get; set; }
        public virtual DbSet<SubCategoriaUso> subCategorias { get; set; }
        public virtual DbSet<SubSetor> subSetores { get; set; }
        public virtual DbSet<TipoCertidao> tiposCertidao { get; set; }
        public virtual DbSet<TipoParametroCalculo> tiposParametroCalculo { get; set; }
        public virtual DbSet<TipoPedido> tiposPedido { get; set; }
        public virtual DbSet<ValorParametroCalculoCepac> valoresParametroCalculoCepac { get; set; }
        public virtual DbSet<Zona> zonas { get; set; }
        public virtual DbSet<Variavel> variaveis { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

        }
    }
}
