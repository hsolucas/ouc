﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OperacoesUrbanas.Models;

namespace OperacoesUrbanas.Controllers
{
    public class OpUrbanasController : ApiController
    {
        private OperacoesUrbanasContext db = new OperacoesUrbanasContext();

        // GET: api/OpUrbanas
        public IQueryable<OperacaoUrbana> GetoperacoesUrbanas()
        {
            return db.operacoesUrbanas;
        }

        // GET: api/OpUrbanas/5
        [ResponseType(typeof(OperacaoUrbana))]
        public IHttpActionResult GetOperacaoUrbana(int id)
        {
            OperacaoUrbana operacaoUrbana = db.operacoesUrbanas.Find(id);
            if (operacaoUrbana == null)
            {
                return NotFound();
            }

            return Ok(operacaoUrbana);
        }

        // PUT: api/OpUrbanas/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutOperacaoUrbana(int id, OperacaoUrbana operacaoUrbana)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != operacaoUrbana.id)
            {
                return BadRequest();
            }

            db.Entry(operacaoUrbana).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OperacaoUrbanaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/OpUrbanas
        [ResponseType(typeof(OperacaoUrbana))]
        public IHttpActionResult PostOperacaoUrbana(OperacaoUrbana operacaoUrbana)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.operacoesUrbanas.Add(operacaoUrbana);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = operacaoUrbana.id }, operacaoUrbana);
        }

        // DELETE: api/OpUrbanas/5
        [ResponseType(typeof(OperacaoUrbana))]
        public IHttpActionResult DeleteOperacaoUrbana(int id)
        {
            OperacaoUrbana operacaoUrbana = db.operacoesUrbanas.Find(id);
            if (operacaoUrbana == null)
            {
                return NotFound();
            }

            db.operacoesUrbanas.Remove(operacaoUrbana);
            db.SaveChanges();

            return Ok(operacaoUrbana);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OperacaoUrbanaExists(int id)
        {
            return db.operacoesUrbanas.Count(e => e.id == id) > 0;
        }
    }
}