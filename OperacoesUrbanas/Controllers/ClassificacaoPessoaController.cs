﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OperacoesUrbanas.Models;

namespace OperacoesUrbanas.Controllers
{
    public class ClassificacaoPessoaController : ApiController
    {
        private OperacoesUrbanasContext db = new OperacoesUrbanasContext();

        // GET: api/ClassificacaoPessoa
        public IQueryable<ClassificacaoPessoa> GetclassificacoesPessoa()
        {
            return db.classificacoesPessoa;
        }

        // GET: api/ClassificacaoPessoa/5
        [ResponseType(typeof(ClassificacaoPessoa))]
        public IHttpActionResult GetClassificacaoPessoa(int id)
        {
            ClassificacaoPessoa classificacaoPessoa = db.classificacoesPessoa.Find(id);
            if (classificacaoPessoa == null)
            {
                return NotFound();
            }

            return Ok(classificacaoPessoa);
        }

        // PUT: api/ClassificacaoPessoa/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutClassificacaoPessoa(int id, ClassificacaoPessoa classificacaoPessoa)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != classificacaoPessoa.id)
            {
                return BadRequest();
            }

            db.Entry(classificacaoPessoa).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClassificacaoPessoaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ClassificacaoPessoa
        [ResponseType(typeof(ClassificacaoPessoa))]
        public IHttpActionResult PostClassificacaoPessoa(ClassificacaoPessoa classificacaoPessoa)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.classificacoesPessoa.Add(classificacaoPessoa);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = classificacaoPessoa.id }, classificacaoPessoa);
        }

        // DELETE: api/ClassificacaoPessoa/5
        [ResponseType(typeof(ClassificacaoPessoa))]
        public IHttpActionResult DeleteClassificacaoPessoa(int id)
        {
            ClassificacaoPessoa classificacaoPessoa = db.classificacoesPessoa.Find(id);
            if (classificacaoPessoa == null)
            {
                return NotFound();
            }

            db.classificacoesPessoa.Remove(classificacaoPessoa);
            db.SaveChanges();

            return Ok(classificacaoPessoa);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ClassificacaoPessoaExists(int id)
        {
            return db.classificacoesPessoa.Count(e => e.id == id) > 0;
        }
    }
}