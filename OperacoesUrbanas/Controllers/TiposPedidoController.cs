﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OperacoesUrbanas.Models;

namespace OperacoesUrbanas.Controllers
{
    public class TiposPedidoController : ApiController
    {
        private OperacoesUrbanasContext db = new OperacoesUrbanasContext();

        // GET: api/TiposPedido
        public IQueryable<TipoPedido> GettiposPedido()
        {
            return db.tiposPedido;
        }

        // GET: api/TiposPedido/5
        [ResponseType(typeof(TipoPedido))]
        public IHttpActionResult GetTipoPedido(int id)
        {
            TipoPedido tipoPedido = db.tiposPedido.Find(id);
            if (tipoPedido == null)
            {
                return NotFound();
            }

            return Ok(tipoPedido);
        }

        // PUT: api/TiposPedido/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTipoPedido(int id, TipoPedido tipoPedido)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tipoPedido.id)
            {
                return BadRequest();
            }

            db.Entry(tipoPedido).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TipoPedidoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TiposPedido
        [ResponseType(typeof(TipoPedido))]
        public IHttpActionResult PostTipoPedido(TipoPedido tipoPedido)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.tiposPedido.Add(tipoPedido);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tipoPedido.id }, tipoPedido);
        }

        // DELETE: api/TiposPedido/5
        [ResponseType(typeof(TipoPedido))]
        public IHttpActionResult DeleteTipoPedido(int id)
        {
            TipoPedido tipoPedido = db.tiposPedido.Find(id);
            if (tipoPedido == null)
            {
                return NotFound();
            }

            db.tiposPedido.Remove(tipoPedido);
            db.SaveChanges();

            return Ok(tipoPedido);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TipoPedidoExists(int id)
        {
            return db.tiposPedido.Count(e => e.id == id) > 0;
        }
    }
}