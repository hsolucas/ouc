﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OperacoesUrbanas.Models;

namespace OperacoesUrbanas.Controllers
{
    public class VariaveisAPIController : ApiController
    {
        private OperacoesUrbanasContext db = new OperacoesUrbanasContext();

        // GET: api/VariaveisAPI
        public IQueryable<Variavel> GetVariavels()
        {
            return db.variaveis;
        }

        // GET: api/VariaveisAPI/5
        [ResponseType(typeof(Variavel))]
        public IHttpActionResult GetVariavel(int id)
        {
            Variavel variavel = db.variaveis.Find(id);
            if (variavel == null)
            {
                return NotFound();
            }

            return Ok(variavel);
        }

        // PUT: api/VariaveisAPI/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutVariavel(int id, Variavel variavel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != variavel.id)
            {
                return BadRequest();
            }

            db.Entry(variavel).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VariavelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/VariaveisAPI
        [ResponseType(typeof(Variavel))]
        public IHttpActionResult PostVariavel(Variavel variavel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.variaveis.Add(variavel);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = variavel.id }, variavel);
        }

        // DELETE: api/VariaveisAPI/5
        [ResponseType(typeof(Variavel))]
        public IHttpActionResult DeleteVariavel(int id)
        {
            Variavel variavel = db.variaveis.Find(id);
            if (variavel == null)
            {
                return NotFound();
            }

            db.variaveis.Remove(variavel);
            db.SaveChanges();

            return Ok(variavel);
        }

        [HttpGet, Route("api/variaveis/{nome}/{idSubSetor}/{idCategoriaUso}")]
        [ResponseType(typeof(Variavel))]
        public IHttpActionResult GetVariavel(string nome, int idSubSetor, int? idCategoriaUso)
        {
            try
            {
                Variavel variavel = db.variaveis.Single(v =>
                    v.idCategoriaUso == idCategoriaUso &&
                    v.idSubSetor == idSubSetor &&
                    v.nome.Equals(nome)
                );
                if(variavel == null)
                {
                    return NotFound();
                }

                return Ok(variavel);
            }catch(Exception ex)
            {
                return InternalServerError();
            }
        }

        [HttpGet, Route("api/variaveis/{nome}/{idSubSetor}")]
        [ResponseType(typeof(Variavel))]
        public IHttpActionResult GetVariavel(string nome, int idSubSetor)
        {
            return GetVariavel(nome, idSubSetor, null);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VariavelExists(int id)
        {
            return db.variaveis.Count(e => e.id == id) > 0;
        }
    }
}