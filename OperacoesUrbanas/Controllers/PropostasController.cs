﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OperacoesUrbanas.Models;
using ReportGenerator.Main;
using OperacoesUrbanas.Enums;
using OperacoesUrbanas.Models.FormulariosRelatorios;
using Newtonsoft.Json;

namespace OperacoesUrbanas.Controllers
{
    public class PropostasController : Controller
    {
        private OperacoesUrbanasContext db = new OperacoesUrbanasContext();

        //GET : Propostas/Nova
        [ActionName("Nova")]
        public ActionResult AbrirNovaProposta()
        {
            return View("Proposta");
        }

        //GET : Propostas/Formula
        [ActionName("Formula")]
        public ActionResult CadastrarFormulas()
        {
            return View("Parametros/ParametroOperacaoUrbana");
        }

        [ActionName("Pendentes")]
        public ActionResult MostrarPendentes()
        {
            var pendentes = ConsultarPropostas((int)StatusEnum.CALCULADA).OrderBy(p => p.dt_cadastro);
            ViewData.Model = pendentes;
            return View("Index");
        }

        private IEnumerable<Proposta> ConsultarPropostas(int? idStatus, int? idUsuario = null)
        {
            try
            {
                return db.propostas.Where(
                    p => p.idStatus == idStatus && p.id_user_cadastro == idUsuario
                ).Include(p => p.subCategoriaUso).Include(p => p.subSetor.setor.operacaoUrbana).Include(p => p.tipoCertidao).Include(p => p.tipoPedido).Include(p => p.zonaAtual).Include(p => p.status);
            }catch(Exception ex)
            {
                Log.Logger.Write(ex);
                return new List<Proposta>();
            }

        }

        // GET: Propostas
        public ActionResult Index()
        {
            var propostas = db.propostas.Include(p => p.contato).Include(p => p.interessado).Include(p => p.proprietarioCepac).Include(p => p.subCategoriaUso).Include(p => p.subSetor).Include(p => p.tipoCertidao).Include(p => p.tipoPedido).Include(p => p.zonaAnterior).Include(p => p.zonaAtual);
            return View(propostas.ToList());
        }

        // GET: Propostas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Proposta proposta = db.propostas.Find(id);
            if (proposta == null)
            {
                return HttpNotFound();
            }
            return View(proposta);
        }

        // GET: Propostas/Create
        public ActionResult Create()
        {
            ViewBag.idContato = new SelectList(db.pessoas, "id", "nome");
            ViewBag.idInteressado = new SelectList(db.pessoas, "id", "nome");
            ViewBag.idProprietarioCepac = new SelectList(db.pessoas, "id", "nome");
            ViewBag.idSubCategoriaUso = new SelectList(db.subCategorias, "id", "descricao");
            ViewBag.idSubSetor = new SelectList(db.subSetores, "id", "nome");
            ViewBag.idTipoCertidao = new SelectList(db.tiposCertidao, "id", "nome");
            ViewBag.idTipoPedido = new SelectList(db.tiposPedido, "id", "nome");
            ViewBag.idZonaAnterior = new SelectList(db.zonas, "id", "descricao");
            ViewBag.idZonaAtual = new SelectList(db.zonas, "id", "descricao");
            return View();
        }

        [HttpPost]
        public ActionResult SalvarProposta(Proposta proposta)
        {
            var imoveis = Request.Params.Get("imoveis");
            var calculo = Request.Params.Get("calculoOpUrbana");

            return null;
        }


        // POST: Propostas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,idTipoCertidao,idTipoPedido,idSubSetor,idZonaAtual,idZonaAnterior,idSubCategoriaUso,_acca,idInteressado,idProprietarioCepac,idContato,_pedidoAlvara,taxaOcupacaoMaxima,gabarito,caBasico,caProjeto,areaRealTotal,areaEscrituraTotal,_desapropriacao,_potencialAdicional,qtdeCepac,calculoAcca,dt_cadastro,dt_alteracao,id_user_cadastro,id_user_alteracao")] Proposta proposta)
        {
            if (ModelState.IsValid)
            {
                db.propostas.Add(proposta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idContato = new SelectList(db.pessoas, "id", "nome", proposta.idContato);
            ViewBag.idInteressado = new SelectList(db.pessoas, "id", "nome", proposta.idInteressado);
            ViewBag.idProprietarioCepac = new SelectList(db.pessoas, "id", "nome", proposta.idProprietarioCepac);
            ViewBag.idSubCategoriaUso = new SelectList(db.subCategorias, "id", "descricao", proposta.idSubCategoriaUso);
            ViewBag.idSubSetor = new SelectList(db.subSetores, "id", "nome", proposta.idSubSetor);
            ViewBag.idTipoCertidao = new SelectList(db.tiposCertidao, "id", "nome", proposta.idTipoCertidao);
            ViewBag.idTipoPedido = new SelectList(db.tiposCertidao, "id", "nome", proposta.idTipoPedido);
            ViewBag.idZonaAnterior = new SelectList(db.zonas, "id", "descricao", proposta.idZonaAnterior);
            ViewBag.idZonaAtual = new SelectList(db.zonas, "id", "descricao", proposta.idZonaAtual);
            return View(proposta);
        }

        // GET: Propostas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Proposta proposta = db.propostas.Find(id);
            if (proposta == null)
            {
                return HttpNotFound();
            }
            ViewBag.idContato = new SelectList(db.pessoas, "id", "nome", proposta.idContato);
            ViewBag.idInteressado = new SelectList(db.pessoas, "id", "nome", proposta.idInteressado);
            ViewBag.idProprietarioCepac = new SelectList(db.pessoas, "id", "nome", proposta.idProprietarioCepac);
            ViewBag.idSubCategoriaUso = new SelectList(db.subCategorias, "id", "descricao", proposta.idSubCategoriaUso);
            ViewBag.idSubSetor = new SelectList(db.subSetores, "id", "nome", proposta.idSubSetor);
            ViewBag.idTipoCertidao = new SelectList(db.tiposCertidao, "id", "nome", proposta.idTipoCertidao);
            ViewBag.idTipoPedido = new SelectList(db.tiposCertidao, "id", "nome", proposta.idTipoPedido);
            ViewBag.idZonaAnterior = new SelectList(db.zonas, "id", "descricao", proposta.idZonaAnterior);
            ViewBag.idZonaAtual = new SelectList(db.zonas, "id", "descricao", proposta.idZonaAtual);
            return View(proposta);
        }

        // POST: Propostas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,idTipoCertidao,idTipoPedido,idSubSetor,idZonaAtual,idZonaAnterior,idSubCategoriaUso,_acca,idInteressado,idProprietarioCepac,idContato,_pedidoAlvara,taxaOcupacaoMaxima,gabarito,caBasico,caProjeto,areaRealTotal,areaEscrituraTotal,_desapropriacao,_potencialAdicional,qtdeCepac,calculoAcca,dt_cadastro,dt_alteracao,id_user_cadastro,id_user_alteracao")] Proposta proposta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(proposta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idContato = new SelectList(db.pessoas, "id", "nome", proposta.idContato);
            ViewBag.idInteressado = new SelectList(db.pessoas, "id", "nome", proposta.idInteressado);
            ViewBag.idProprietarioCepac = new SelectList(db.pessoas, "id", "nome", proposta.idProprietarioCepac);
            ViewBag.idSubCategoriaUso = new SelectList(db.subCategorias, "id", "descricao", proposta.idSubCategoriaUso);
            ViewBag.idSubSetor = new SelectList(db.subSetores, "id", "nome", proposta.idSubSetor);
            ViewBag.idTipoCertidao = new SelectList(db.tiposCertidao, "id", "nome", proposta.idTipoCertidao);
            ViewBag.idTipoPedido = new SelectList(db.tiposCertidao, "id", "nome", proposta.idTipoPedido);
            ViewBag.idZonaAnterior = new SelectList(db.zonas, "id", "descricao", proposta.idZonaAnterior);
            ViewBag.idZonaAtual = new SelectList(db.zonas, "id", "descricao", proposta.idZonaAtual);
            return View(proposta);
        }

        // GET: Propostas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Proposta proposta = db.propostas.Find(id);
            if (proposta == null)
            {
                return HttpNotFound();
            }
            return View(proposta);
        }

        // POST: Propostas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Proposta proposta = db.propostas.Find(id);
            db.propostas.Remove(proposta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public void ImprimirSimulacao(Calculo calculo)
        {
            string viewName = null;
            switch (calculo.idOperacaoUrbana)
            {
                case (int)OperacaoUrbanaEnum.AGUA_ESPRAIADA:
                    viewName = "Calculos/Simulacoes/SimulacaoAguaEspraiada";
                break;
                case (int)OperacaoUrbanaEnum.FARIA_LIMA:
                    viewName = "Calculos/Simulacoes/SimulacaoFariaLima";
                break;
                default:
                    return;
            }
            string html = GetHtmlViewAsString(viewName, calculo);
            ReportPDFGenerator.ExportHtmlToPdf(html, "simulacao_calculo.pdf", "~/Content/Simulacao.css", false, PDFOrientation.RETRATO);
        }

        [HttpPost]
        public void ImprimirFrmRosto(Rosto formulario)
        {
            formulario.listaContatos = JsonConvert.DeserializeObject<List<Pessoa>>(formulario.contatos);
            formulario.listaProprietarios = JsonConvert.DeserializeObject<List<Pessoa>>(formulario.proprietarios);
            formulario.listaInteressados = JsonConvert.DeserializeObject<List<Pessoa>>(formulario.interessados);

            string viewName = "Calculos/Formularios/Rosto";
            string html = GetHtmlViewAsString(viewName, formulario);
            ReportPDFGenerator.ExportHtmlToPdf(html, "formulario_rosto.pdf", "~/Content/Rosto.css", true, PDFOrientation.RETRATO);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        private string GetHtmlViewAsString(string nomeView, Object model)
        {
            ViewData.Model = model;
            System.IO.StringWriter sw = new System.IO.StringWriter();

            ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, nomeView);
            ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
            viewResult.View.Render(viewContext, sw);
            viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
            string strhtml = sw.GetStringBuilder().ToString();
            sw.Close();
            return strhtml;
        }

    }
}
