﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OperacoesUrbanas.Models;
using Log;

namespace OperacoesUrbanas.Controllers
{
    public class PropostasAPIController : ApiController
    {
        private OperacoesUrbanasContext db = new OperacoesUrbanasContext();

        // GET: api/PropostasAPI
        public IQueryable<Proposta> Getpropostas()
        {
            return db.propostas;
        }

        [HttpPost]
        [Route("api/propostas/simularcalculo")]
        public void SimularCalculo([FromBody] List<ValorParametroCalculoCepac> valores)
        {
            Redirect("/Views/Propostas/Index");
        }

        // GET: api/PropostasAPI/5
        [ResponseType(typeof(Proposta))]
        public IHttpActionResult GetProposta(int id)
        {
            Proposta proposta = db.propostas.Find(id);
            if (proposta == null)
            {
                return NotFound();
            }

            return Ok(proposta);
        }

        // PUT: api/PropostasAPI/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutProposta(int id, Proposta proposta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != proposta.id)
            {
                return BadRequest();
            }

            db.Entry(proposta).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PropostaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PropostasAPI
        [ResponseType(typeof(Proposta))]
        public IHttpActionResult PostProposta(Proposta proposta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.propostas.Add(proposta);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = proposta.id }, proposta);
        }

        // DELETE: api/PropostasAPI/5
        [ResponseType(typeof(Proposta))]
        public IHttpActionResult DeleteProposta(int id)
        {
            Proposta proposta = db.propostas.Find(id);
            if (proposta == null)
            {
                return NotFound();
            }

            db.propostas.Remove(proposta);
            db.SaveChanges();

            return Ok(proposta);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PropostaExists(int id)
        {
            return db.propostas.Count(e => e.id == id) > 0;
        }

        [HttpPost]
        [Route("api/propostas/finalizar")]
        [ResponseType(typeof(Proposta))]
        public IHttpActionResult SalvarProposta([FromBody] Proposta proposta)
        {
            proposta.dt_cadastro = DateTime.Now;
            try
            {
                db.propostas.Add(proposta);
                db.SaveChanges();

                SalvarImoveisProposta(proposta, true);

                return Ok(proposta);
            }
            catch (Exception ex)
            {
                Log.Logger.Write(ex);
                RemoverProposta(proposta);
                return InternalServerError();
            }
        }

        [HttpPost]
        [Route("api/propostas/encerrar")]
        [ResponseType(typeof(Proposta))]
        public IHttpActionResult EncerrarProposta([FromBody] Proposta proposta)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            proposta.dt_alteracao = DateTime.Now;

            db.Entry(proposta).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
                SalvarImoveisProposta(proposta, false);
                SalvarInteressadosProposta(proposta);
                SalvarProprietariosCepacProposta(proposta);
                SalvarContatosProposta(proposta);

                return Ok(proposta);
            }
            catch (DbUpdateConcurrencyException ex)
            {
                if (!PropostaExists(proposta.id))
                {
                    return NotFound();
                }
                else
                {
                    Logger.Write(ex);
                    throw;
                }
            }
        }


        [HttpPost]
        [Route("api/propostas/remover")]
        public IHttpActionResult RemoverProposta(Proposta proposta)
        {
            try
            {
                Proposta propostaRem = db.propostas.Find(proposta.id);
                if (propostaRem == null)
                {
                    return NotFound();
                }

                db.propostas.Remove(propostaRem);
                db.SaveChanges();

                return Ok(propostaRem);
            }catch(Exception ex)
            {
                return InternalServerError();
            }

        }

        private void SalvarImoveisProposta(Proposta proposta, bool associar)
        {
            foreach (Imovel i in proposta.imoveis)
            {
                try
                {
                    if (i.id > 0)
                    {
                        db.Entry(i).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else
                    {
                        i.dataCadastro = DateTime.Now;
                        db.imoveis.Add(i);
                        db.SaveChanges();
                    }

                    if (associar)
                    {
                        ImovelProposta imovelProposta = new ImovelProposta();
                        imovelProposta.idImovel = i.id;
                        imovelProposta.idProposta = proposta.id;
                        db.imoveisProposta.Add(imovelProposta);
                        db.SaveChanges();
                    }

                }catch(Exception ex)
                {
                    //Rollback
                    throw ex;
                }

            }
        }

        private void SalvarPessoaProposta(Pessoa p)
        {

            if (p.id > 0)
            {
                p.dataAlteracao = DateTime.Now;
                db.Entry(p).State = EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                p.dataCadastro = DateTime.Now;
                db.pessoas.Add(p);
                db.SaveChanges();
            }

        }

        private void SalvarInteressadosProposta(Proposta proposta)
        {
            foreach (Pessoa p in proposta.interessados)
            {
                try
                {
                    SalvarPessoaProposta(p);

                    InteressadoProposta interessadoProposta = new InteressadoProposta();
                    interessadoProposta.idInteressado = p.id;
                    interessadoProposta.idProposta = proposta.id;
                    db.interessadosProposta.Add(interessadoProposta);
                    db.SaveChanges();

                }
                catch (Exception ex)
                {
                    //Rollback
                    throw ex;
                }

            }
        }

        private void SalvarProprietariosCepacProposta(Proposta proposta)
        {
            foreach (Pessoa p in proposta.proprietarios)
            {
                try
                {
                    SalvarPessoaProposta(p);

                    ProprietarioCepacProposta assoc = new ProprietarioCepacProposta();
                    assoc.idProprietario = p.id;
                    assoc.idProposta = proposta.id;
                    db.proprietariosCepacProposta.Add(assoc);
                    db.SaveChanges();

                }
                catch (Exception ex)
                {
                    //Rollback
                    throw ex;
                }

            }
        }

        private void SalvarContatosProposta(Proposta proposta)
        {
            foreach (Pessoa p in proposta.proprietarios)
            {
                try
                {
                    SalvarPessoaProposta(p);

                    ContatoProposta assoc = new ContatoProposta();
                    assoc.idContato = p.id;
                    assoc.idProposta = proposta.id;
                    db.contatosProposta.Add(assoc);
                    db.SaveChanges();

                }
                catch (Exception ex)
                {
                    //Rollback
                    throw ex;
                }

            }
        }

    }
}