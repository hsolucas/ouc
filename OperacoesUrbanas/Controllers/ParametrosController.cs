﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OperacoesUrbanas.Models;

namespace OperacoesUrbanas.Controllers
{
    public class ParametrosController : ApiController
    {
        private OperacoesUrbanasContext db = new OperacoesUrbanasContext();

        // GET: api/Parametros
        public IQueryable<ParametroCalculoCepac> GetparametrosCalculoCepac()
        {
            return db.parametrosCalculoCepac;
        }

        // GET: api/Parametros/5
        [ResponseType(typeof(ParametroCalculoCepac))]
        public IHttpActionResult GetParametroCalculoCepac(int id)
        {
            ParametroCalculoCepac parametroCalculoCepac = db.parametrosCalculoCepac.Find(id);
            if (parametroCalculoCepac == null)
            {
                return NotFound();
            }

            return Ok(parametroCalculoCepac);
        }

        // PUT: api/Parametros/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutParametroCalculoCepac(int id, ParametroCalculoCepac parametroCalculoCepac)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != parametroCalculoCepac.id)
            {
                return BadRequest();
            }

            db.Entry(parametroCalculoCepac).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ParametroCalculoCepacExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Parametros
        [ResponseType(typeof(ParametroCalculoCepac))]
        public IHttpActionResult PostParametroCalculoCepac(ParametroCalculoCepac parametroCalculoCepac)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.parametrosCalculoCepac.Add(parametroCalculoCepac);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = parametroCalculoCepac.id }, parametroCalculoCepac);
        }

        // DELETE: api/Parametros/5
        [ResponseType(typeof(ParametroCalculoCepac))]
        public IHttpActionResult DeleteParametroCalculoCepac(int id)
        {
            ParametroCalculoCepac parametroCalculoCepac = db.parametrosCalculoCepac.Find(id);
            if (parametroCalculoCepac == null)
            {
                return NotFound();
            }

            db.parametrosCalculoCepac.Remove(parametroCalculoCepac);
            db.SaveChanges();

            return Ok(parametroCalculoCepac);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ParametroCalculoCepacExists(int id)
        {
            return db.parametrosCalculoCepac.Count(e => e.id == id) > 0;
        }
    }
}