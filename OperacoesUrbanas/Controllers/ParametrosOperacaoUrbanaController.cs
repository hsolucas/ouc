﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OperacoesUrbanas.Models;
using OperacoesUrbanas.Enums;

namespace OperacoesUrbanas.Controllers
{
    public class ParametrosOperacaoUrbanaController : ApiController
    {
        private OperacoesUrbanasContext db = new OperacoesUrbanasContext();

        // GET: api/ParametrosOperacaoUrbana
        public IQueryable<ParametroOperacaoUrbana> GetParametroOperacaoUrbanas()
        {
            return db.parametrosOperacaoUrbana;
        }

        [Route("api/parametrosoperacaourbana/var/{idOperacaoUrbana}")]
        [ResponseType(typeof(ParametroOperacaoUrbana))]
        public IHttpActionResult GetParametrosOperacaoUrbanaTipoVar(int idOperacaoUrbana)
        {
            try
            {

                List<ParametroCalculoCepac> parametrosVar = db.parametrosCalculoCepac.Where(
                    p => p.tipoParametroCalculo.id == (int)TipoParametroEnum.VARIAVEL
                    ).ToList();

                List<ParametroCalculoCepac> parametrosVarOpUrbana = new List<ParametroCalculoCepac>();

                foreach(ParametroCalculoCepac p in parametrosVar)
                {
                    if (IsParametroOperacaoUrbana(p.id, idOperacaoUrbana))
                    {
                        parametrosVarOpUrbana.Add(p);
                    }
                }

                return Json(parametrosVarOpUrbana);

            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [Route("api/parametros")]
        [ResponseType(typeof(ParametroCalculoCepac))]
        public IHttpActionResult GetParametros()
        {
            try
            {

                List<ParametroCalculoCepac> parametros = db.parametrosCalculoCepac.ToList();

                return Json(parametros);

            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }


        [Route("api/parametrosoperacaourbana/operacaourbana/{idOperacaoUrbana}")]
        [ResponseType(typeof(ParametroOperacaoUrbana))]
        public IHttpActionResult GetParametrosOperacaoUrbana(int idOperacaoUrbana)
        {
            try
            {

                List<ParametroOperacaoUrbana> parametros = 
                    db.parametrosOperacaoUrbana.Where(po => po.idOperacaoUrbana == idOperacaoUrbana).ToList();

                return Json(parametros);

            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }



        [Route("api/parametrosoperacaourbana/operacaourbana/{idOperacaoUrbana}/{idParametroPai}")]
        [ResponseType(typeof(ParametroOperacaoUrbana))]
        public IHttpActionResult GetParametrosOperacaoUrbana(int idOperacaoUrbana, int idParametroPai)
        {
            try
            {
                return Json(db.parametrosOperacaoUrbana.Where(
                    po => po.idOperacaoUrbana == idOperacaoUrbana && po.idParametroPai == idParametroPai
                ));
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        // GET: api/ParametrosOperacaoUrbana/5
        [ResponseType(typeof(ParametroOperacaoUrbana))]
        public IHttpActionResult GetParametroOperacaoUrbana(int id)
        {
            ParametroOperacaoUrbana parametroOperacaoUrbana = db.parametrosOperacaoUrbana.Find(id);
            if (parametroOperacaoUrbana == null)
            {
                return NotFound();
            }

            return Ok(parametroOperacaoUrbana);
        }

        // PUT: api/ParametrosOperacaoUrbana/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutParametroOperacaoUrbana(int id, ParametroOperacaoUrbana parametroOperacaoUrbana)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != parametroOperacaoUrbana.id)
            {
                return BadRequest();
            }

            db.Entry(parametroOperacaoUrbana).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ParametroOperacaoUrbanaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ParametrosOperacaoUrbana
        [ResponseType(typeof(ParametroOperacaoUrbana))]
        public IHttpActionResult PostParametroOperacaoUrbana(ParametroOperacaoUrbana parametroOperacaoUrbana)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.parametrosOperacaoUrbana.Add(parametroOperacaoUrbana);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = parametroOperacaoUrbana.id }, parametroOperacaoUrbana);
        }

        // DELETE: api/ParametrosOperacaoUrbana/5
        [ResponseType(typeof(ParametroOperacaoUrbana))]
        public IHttpActionResult DeleteParametroOperacaoUrbana(int id)
        {
            ParametroOperacaoUrbana parametroOperacaoUrbana = db.parametrosOperacaoUrbana.Find(id);
            if (parametroOperacaoUrbana == null)
            {
                return NotFound();
            }

            db.parametrosOperacaoUrbana.Remove(parametroOperacaoUrbana);
            db.SaveChanges();

            return Ok(parametroOperacaoUrbana);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ParametroOperacaoUrbanaExists(int id)
        {
            return db.parametrosOperacaoUrbana.Count(e => e.id == id) > 0;
        }

        private bool IsParametroOperacaoUrbana(int idParametro, int idOperacaoUrbana)
        {
            return db.parametrosOperacaoUrbana.Where(
                po => po.idParametro == idParametro && po.idOperacaoUrbana == idOperacaoUrbana
            ).Count() > 0;
        }
    }
}