﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OperacoesUrbanas.Models;

namespace OperacoesUrbanas.Controllers
{
    public class SubSetoresController : ApiController
    {
        private OperacoesUrbanasContext db = new OperacoesUrbanasContext();

        // GET: api/SubSetores
        public IQueryable<SubSetor> GetsubSetores()
        {
            return db.subSetores;
        }

        [Route("api/subsetores/setor/{idSetor}")]
        [ResponseType(typeof(Setor))]
        public IHttpActionResult GetsetoresOpUrbana(int idSetor)
        {
            try
            {
                return Json(db.subSetores.Where(ss => ss.idSetor == idSetor));
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        // GET: api/SubSetores/5
        [ResponseType(typeof(SubSetor))]
        public IHttpActionResult GetSubSetor(int id)
        {
            SubSetor subSetor = db.subSetores.Find(id);
            if (subSetor == null)
            {
                return NotFound();
            }

            return Ok(subSetor);
        }

        // PUT: api/SubSetores/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSubSetor(int id, SubSetor subSetor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != subSetor.id)
            {
                return BadRequest();
            }

            db.Entry(subSetor).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubSetorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/SubSetores
        [ResponseType(typeof(SubSetor))]
        public IHttpActionResult PostSubSetor(SubSetor subSetor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.subSetores.Add(subSetor);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = subSetor.id }, subSetor);
        }

        // DELETE: api/SubSetores/5
        [ResponseType(typeof(SubSetor))]
        public IHttpActionResult DeleteSubSetor(int id)
        {
            SubSetor subSetor = db.subSetores.Find(id);
            if (subSetor == null)
            {
                return NotFound();
            }

            db.subSetores.Remove(subSetor);
            db.SaveChanges();

            return Ok(subSetor);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SubSetorExists(int id)
        {
            return db.subSetores.Count(e => e.id == id) > 0;
        }
    }
}