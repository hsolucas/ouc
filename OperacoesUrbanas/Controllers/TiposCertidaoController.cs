﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OperacoesUrbanas.Models;

namespace OperacoesUrbanas.Controllers
{
    public class TiposCertidaoController : ApiController
    {
        private OperacoesUrbanasContext db = new OperacoesUrbanasContext();

        // GET: api/TiposCertidao
        public IQueryable<TipoCertidao> GettiposCertidao()
        {
            return db.tiposCertidao;
        }

        // GET: api/TiposCertidao/5
        [ResponseType(typeof(TipoCertidao))]
        public IHttpActionResult GetTipoCertidao(int id)
        {
            TipoCertidao tipoCertidao = db.tiposCertidao.Find(id);
            if (tipoCertidao == null)
            {
                return NotFound();
            }

            return Ok(tipoCertidao);
        }

        // PUT: api/TiposCertidao/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTipoCertidao(int id, TipoCertidao tipoCertidao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tipoCertidao.id)
            {
                return BadRequest();
            }

            db.Entry(tipoCertidao).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TipoCertidaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TiposCertidao
        [ResponseType(typeof(TipoCertidao))]
        public IHttpActionResult PostTipoCertidao(TipoCertidao tipoCertidao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.tiposCertidao.Add(tipoCertidao);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tipoCertidao.id }, tipoCertidao);
        }

        // DELETE: api/TiposCertidao/5
        [ResponseType(typeof(TipoCertidao))]
        public IHttpActionResult DeleteTipoCertidao(int id)
        {
            TipoCertidao tipoCertidao = db.tiposCertidao.Find(id);
            if (tipoCertidao == null)
            {
                return NotFound();
            }

            db.tiposCertidao.Remove(tipoCertidao);
            db.SaveChanges();

            return Ok(tipoCertidao);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TipoCertidaoExists(int id)
        {
            return db.tiposCertidao.Count(e => e.id == id) > 0;
        }
    }
}