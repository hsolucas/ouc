﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OperacoesUrbanas.Models;

namespace OperacoesUrbanas.Controllers
{
    public class ImoveisAPIController : ApiController
    {
        private OperacoesUrbanasContext db = new OperacoesUrbanasContext();

        // GET: api/ImoveisAPI
        public IQueryable<Imovel> Getimoveis()
        {
            return db.imoveis;
        }

        // GET: api/ImoveisAPI/5
        [ResponseType(typeof(Imovel))]
        public IHttpActionResult GetImovel(int id)
        {
            Imovel imovel = db.imoveis.Find(id);
            if (imovel == null)
            {
                return NotFound();
            }

            return Ok(imovel);
        }

        [ResponseType(typeof(Imovel))]
        public IHttpActionResult GetImovelByContribuinte(string contribuinte)
        {
            Imovel imovel = db.imoveis.FirstOrDefault(i => i.contribuinte.Equals(contribuinte));
            if (imovel == null)
            {
                return NotFound();
            }

            return Ok(imovel);
        }

        [HttpPost]
        [Route("api/imoveis/proposta/{idProposta}")]
        [ResponseType(typeof(List<ImovelProposta>))]
        public IHttpActionResult GetImoveisProposta(int? idProposta)
        {
            if(idProposta == null)
            {
                return StatusCode(HttpStatusCode.BadRequest);
            }

            try
            {
                return Ok(db.imoveisProposta.Include(i => i.imovel).Where(i => i.idProposta == idProposta).ToList());
            }catch(Exception ex)
            {
                Log.Logger.Write(ex);
                return StatusCode(HttpStatusCode.ServiceUnavailable);
            }
            
        }

        // PUT: api/ImoveisAPI/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutImovel(int id, Imovel imovel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != imovel.id)
            {
                return BadRequest();
            }

            db.Entry(imovel).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ImovelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpPost]
        [Route("api/imoveis/salvar")]
        [ResponseType(typeof(Imovel))]
        public IHttpActionResult SalvarImovel(Imovel imovel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                if (GetImovelByContribuinte(imovel.contribuinte) != null)
                {
                    return Ok(imovel);
                }

                db.imoveis.Add(imovel);
                db.SaveChanges();

                return Ok(imovel);
            }catch(Exception ex)
            {
                return InternalServerError();
            }
        }

        [HttpPost]
        [Route("api/imoveis/proposta/salvar")]
        [ResponseType(typeof(ImovelProposta))]
        public IHttpActionResult SalvarImovelProposta(ImovelProposta imovelProposta)
        {
            try
            {
                db.imoveisProposta.Add(imovelProposta);
                db.SaveChanges();

                return Ok(imovelProposta);
            }
            catch (Exception ex)
            {
                return InternalServerError();
            }
        }

        // DELETE: api/ImoveisAPI/5
        [ResponseType(typeof(Imovel))]
        public IHttpActionResult DeleteImovel(int id)
        {
            Imovel imovel = db.imoveis.Find(id);
            if (imovel == null)
            {
                return NotFound();
            }

            db.imoveis.Remove(imovel);
            db.SaveChanges();

            return Ok(imovel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ImovelExists(int id)
        {
            return db.imoveis.Count(e => e.id == id) > 0;
        }
    }
}