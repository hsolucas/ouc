﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OperacoesUrbanas.Models;

namespace OperacoesUrbanas.Controllers
{
    public class CategoriasUsoController : ApiController
    {
        private OperacoesUrbanasContext db = new OperacoesUrbanasContext();

        // GET: api/CategoriasUso
        public IQueryable<CategoriaUso> GetcategoriasUso()
        {
            return db.categoriasUso;
        }

        // GET: api/CategoriasUso/5
        [ResponseType(typeof(CategoriaUso))]
        public IHttpActionResult GetCategoriaUso(int id)
        {
            CategoriaUso categoriaUso = db.categoriasUso.Find(id);
            if (categoriaUso == null)
            {
                return NotFound();
            }

            return Ok(categoriaUso);
        }

        // PUT: api/CategoriasUso/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCategoriaUso(int id, CategoriaUso categoriaUso)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != categoriaUso.id)
            {
                return BadRequest();
            }

            db.Entry(categoriaUso).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategoriaUsoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CategoriasUso
        [ResponseType(typeof(CategoriaUso))]
        public IHttpActionResult PostCategoriaUso(CategoriaUso categoriaUso)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.categoriasUso.Add(categoriaUso);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = categoriaUso.id }, categoriaUso);
        }

        // DELETE: api/CategoriasUso/5
        [ResponseType(typeof(CategoriaUso))]
        public IHttpActionResult DeleteCategoriaUso(int id)
        {
            CategoriaUso categoriaUso = db.categoriasUso.Find(id);
            if (categoriaUso == null)
            {
                return NotFound();
            }

            db.categoriasUso.Remove(categoriaUso);
            db.SaveChanges();

            return Ok(categoriaUso);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CategoriaUsoExists(int id)
        {
            return db.categoriasUso.Count(e => e.id == id) > 0;
        }
    }
}