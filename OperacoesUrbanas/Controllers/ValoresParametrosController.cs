﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OperacoesUrbanas.Models;

namespace OperacoesUrbanas.Controllers
{
    public class ValoresParametrosController : ApiController
    {
        private OperacoesUrbanasContext db = new OperacoesUrbanasContext();

        // GET: api/ValoresParametros
        public IQueryable<ValorParametroCalculoCepac> GetvaloresParametroCalculoCepac()
        {
            return db.valoresParametroCalculoCepac;
        }

        // GET: api/ValoresParametros/5
        [ResponseType(typeof(ValorParametroCalculoCepac))]
        public IHttpActionResult GetValorParametroCalculoCepac(int id)
        {
            ValorParametroCalculoCepac valorParametroCalculoCepac = db.valoresParametroCalculoCepac.Find(id);
            if (valorParametroCalculoCepac == null)
            {
                return NotFound();
            }

            return Ok(valorParametroCalculoCepac);
        }

        // PUT: api/ValoresParametros/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutValorParametroCalculoCepac(int id, ValorParametroCalculoCepac valorParametroCalculoCepac)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != valorParametroCalculoCepac.id)
            {
                return BadRequest();
            }

            db.Entry(valorParametroCalculoCepac).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ValorParametroCalculoCepacExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ValoresParametros
        [ResponseType(typeof(ValorParametroCalculoCepac))]
        public IHttpActionResult PostValorParametroCalculoCepac(ValorParametroCalculoCepac valorParametroCalculoCepac)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.valoresParametroCalculoCepac.Add(valorParametroCalculoCepac);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = valorParametroCalculoCepac.id }, valorParametroCalculoCepac);
        }

        // DELETE: api/ValoresParametros/5
        [ResponseType(typeof(ValorParametroCalculoCepac))]
        public IHttpActionResult DeleteValorParametroCalculoCepac(int id)
        {
            ValorParametroCalculoCepac valorParametroCalculoCepac = db.valoresParametroCalculoCepac.Find(id);
            if (valorParametroCalculoCepac == null)
            {
                return NotFound();
            }

            db.valoresParametroCalculoCepac.Remove(valorParametroCalculoCepac);
            db.SaveChanges();

            return Ok(valorParametroCalculoCepac);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ValorParametroCalculoCepacExists(int id)
        {
            return db.valoresParametroCalculoCepac.Count(e => e.id == id) > 0;
        }


        [HttpPost]
        [Route("api/valores/gravar")]
        public IHttpActionResult Gravar(ValorParametroCalculoCepac valorParametro)
        {
            try
            {

                valorParametro.dt_cadastro = DateTime.Now;
                valorParametro.idParametro = GetIdParametroByName(valorParametro.nomeParametro);

                db.valoresParametroCalculoCepac.Add(valorParametro);
                
                db.SaveChanges();
                return Ok();

            }
            catch (Exception ex)
            {
                var message = "Oops..." + ex.Message;
                return InternalServerError();
            }
        }

        [HttpPost]
        [Route("api/valores/remover")]
        public IHttpActionResult RemoverValoresProposta(ValorParametroCalculoCepac valorParametro)
        {
            if(valorParametro == null || valorParametro.idProposta == null)
            {
                return BadRequest();
            }

            try
            {
                db.valoresParametroCalculoCepac.RemoveRange(db.valoresParametroCalculoCepac.Where(v => v.idProposta == valorParametro.idProposta));
                db.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {
                var message = "Oops..." + ex.Message;
                return InternalServerError();
            }
        }


        [HttpPost]
        [Route("api/valores/gravarLista")]
        public IHttpActionResult GravarLista(List<ValorParametroCalculoCepac> valores)
        {
            try
            {
                foreach(ValorParametroCalculoCepac val in valores)
                {
                    val.dt_cadastro = DateTime.Now;
                    val.idParametro = GetIdParametroByName(val.nomeParametro);
                    
                    db.valoresParametroCalculoCepac.Add(val);
                }
                db.SaveChanges();
                return Ok();

            }
            catch (Exception ex)
            {
                var message = "Oops..." + ex.Message;
                return InternalServerError();
            }
        }

        private int GetIdParametroByName(string nomeParametro)
        {
            try
            {
                ParametroCalculoCepac parametro = db.parametrosCalculoCepac.SingleOrDefault(p => p.sigla.Equals(nomeParametro));
                return parametro.id;
            }
            catch(Exception ex)
            {
                return 0;
            }
        }

    }
}