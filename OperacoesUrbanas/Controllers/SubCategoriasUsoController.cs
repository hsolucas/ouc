﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OperacoesUrbanas.Models;

namespace OperacoesUrbanas.Controllers
{
    public class SubCategoriasUsoController : ApiController
    {
        private OperacoesUrbanasContext db = new OperacoesUrbanasContext();

        [Route("api/subcategoriasuso/categoriauso/{idCategoria}")]
        [ResponseType(typeof(Setor))]
        public IHttpActionResult GetSubCategoriaUsoPorCategoria(int idCategoria)
        {
            try
            {
                return Json(db.subCategorias.Where(sc => sc.idCategoriaUso == idCategoria));
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        // GET: api/SubCategoriasUso
        public IQueryable<SubCategoriaUso> GetsubCategorias()
        {
            return db.subCategorias;
        }

        // GET: api/SubCategoriasUso/5
        [ResponseType(typeof(SubCategoriaUso))]
        public IHttpActionResult GetSubCategoriaUso(int id)
        {
            SubCategoriaUso subCategoriaUso = db.subCategorias.Find(id);
            if (subCategoriaUso == null)
            {
                return NotFound();
            }

            return Ok(subCategoriaUso);
        }

        // PUT: api/SubCategoriasUso/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSubCategoriaUso(int id, SubCategoriaUso subCategoriaUso)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != subCategoriaUso.id)
            {
                return BadRequest();
            }

            db.Entry(subCategoriaUso).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubCategoriaUsoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/SubCategoriasUso
        [ResponseType(typeof(SubCategoriaUso))]
        public IHttpActionResult PostSubCategoriaUso(SubCategoriaUso subCategoriaUso)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.subCategorias.Add(subCategoriaUso);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = subCategoriaUso.id }, subCategoriaUso);
        }

        // DELETE: api/SubCategoriasUso/5
        [ResponseType(typeof(SubCategoriaUso))]
        public IHttpActionResult DeleteSubCategoriaUso(int id)
        {
            SubCategoriaUso subCategoriaUso = db.subCategorias.Find(id);
            if (subCategoriaUso == null)
            {
                return NotFound();
            }

            db.subCategorias.Remove(subCategoriaUso);
            db.SaveChanges();

            return Ok(subCategoriaUso);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SubCategoriaUsoExists(int id)
        {
            return db.subCategorias.Count(e => e.id == id) > 0;
        }
    }
}