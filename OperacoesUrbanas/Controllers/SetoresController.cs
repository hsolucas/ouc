﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OperacoesUrbanas.Models;

namespace OperacoesUrbanas.Controllers
{
    public class SetoresController : ApiController
    {
        private OperacoesUrbanasContext db = new OperacoesUrbanasContext();

        // GET: api/Setores
        public IQueryable<Setor> Getsetores()
        {
            return db.setores;
        }

        [Route("api/setores/opUrbana/{idOpUrbana}")]
        [ResponseType(typeof(Setor))]
        public IHttpActionResult GetsetoresOpUrbana(int idOpUrbana)
        {
            try
            {
                return Json( db.setores.Where(s => s.id_operacao_urbana == idOpUrbana) );
            }catch(Exception ex)
            {
                return BadRequest();
            }
        }

        // GET: api/Setores/5
        [ResponseType(typeof(Setor))]
        public IHttpActionResult GetSetor(int id)
        {
            Setor setor = db.setores.Find(id);
            if (setor == null)
            {
                return NotFound();
            }

            return Ok(setor);
        }

        // PUT: api/Setores/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSetor(int id, Setor setor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != setor.id)
            {
                return BadRequest();
            }

            db.Entry(setor).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SetorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Setores
        [ResponseType(typeof(Setor))]
        public IHttpActionResult PostSetor(Setor setor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.setores.Add(setor);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = setor.id }, setor);
        }

        // DELETE: api/Setores/5
        [ResponseType(typeof(Setor))]
        public IHttpActionResult DeleteSetor(int id)
        {
            Setor setor = db.setores.Find(id);
            if (setor == null)
            {
                return NotFound();
            }

            db.setores.Remove(setor);
            db.SaveChanges();

            return Ok(setor);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SetorExists(int id)
        {
            return db.setores.Count(e => e.id == id) > 0;
        }
    }
}