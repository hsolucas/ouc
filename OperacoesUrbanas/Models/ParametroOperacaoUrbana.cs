﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OperacoesUrbanas.Models
{
    [Table("ouc.tb_parametro_operacao_urbana")]
    public class ParametroOperacaoUrbana
    {

        [Key]
        [Column("id_parametro_operacao_urbana")]
        public int id { get; set; }

        [Column("id_operacao_urbana")]
        [ForeignKey("operacaoUrbana")]
        public int? idOperacaoUrbana { get; set; }
        public OperacaoUrbana operacaoUrbana { get; set; }

        [Column("id_parametro_calc_cepac")]
        [ForeignKey("parametro")]
        public int? idParametro { get; set; }
        public ParametroCalculoCepac parametro { get; set; }

        [Column("id_parametro_calc_cepac_pai")]
        [ForeignKey("parametroPai")]
        public int? idParametroPai { get; set; }
        public ParametroCalculoCepac parametroPai { get; set; }

        [Column("nr_ordem")]
        public int ordem { get; set; }

    }
}