namespace OperacoesUrbanas.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ouc.tb_pessoa")]
    public class Pessoa
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Pessoa()
        {
            //tb_imovel = new HashSet<Imovel>();
            //tb_proposta = new HashSet<Proposta>();
            //tb_proposta1 = new HashSet<Proposta>();
            //tb_proposta2 = new HashSet<Proposta>();
        }

        [Key]
        [Column("id_pessoa")]
        public int id { get; set; }

        [Column("id_classificacao_pessoa")]
        [ForeignKey("classificacaoPessoa")]
        public int? idClassificacaoPessoa { get; set; }
        public ClassificacaoPessoa classificacaoPessoa { get; set; }

        [Display(Name = "Nome")]
        [StringLength(100)]
        [Column("nm_pessoa")]
        public string nome { get; set; }

        [StringLength(30)]
        [Column("nm_cpf")]
        public string cpf { get; set; }

        [StringLength(30)]
        [Column("nm_cnpj")]
        public string cnpj { get; set; }

        [StringLength(300)]
        [Column("nm_endereco")]
        public string endereco { get; set; }

        [StringLength(30)]
        [Column("nm_rg")]
        public string rg { get; set; }

        [StringLength(30)]
        [Column("nm_telefone")]
        public string telefone { get; set; }

        [StringLength(50)]
        [Column("nm_profissao")]
        public string profissao { get; set; }

        [StringLength(50)]
        [Column("nm_nacionalidade")]
        public string nacionalidade { get; set; }

        [StringLength(30)]
        [Column("nm_telefone_2")]
        public string telefone2 { get; set; }

        [StringLength(100)]
        [Column("nm_email")]
        public string email { get; set; }

        [Column("dt_cadastro")]
        public DateTime? dataCadastro { get; set; }

        [Column("dt_alteracao")]
        public DateTime? dataAlteracao { get; set; }

        [Column("id_user_cadastro")]
        public int? idUsuarioCadastro { get; set; }

        [Column("id_user_alteracao")]
        public int? idUsuarioAlteracao { get; set; }

    }
}
