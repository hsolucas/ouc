namespace OperacoesUrbanas.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ouc.tb_imovel")]
    public class Imovel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Imovel()
        {

        }

        [Key]
        [Column("id_imovel")]
        public int id { get; set; }

        [Column("id_proprietario")]
        [ForeignKey("proprietario")]
        public int? idProprietario { get; set; }
        public Pessoa proprietario { get; set; }

        [Column("nr_area_real")]
        public decimal? areaReal { get; set; }

        [Column("nr_area_escritura")]
        public decimal? areaEscritura { get; set; }

        [StringLength(30)]
        [Column("nr_matricula")]
        public string matricula { get; set; }

        [StringLength(300)]
        [Column("nm_endereco")]
        public string endereco { get; set; }

        [StringLength(30)]
        [Column("cd_contribuinte")]
        public string contribuinte { get; set; }

        [Column("dt_cadastro")]
        public DateTime? dataCadastro { get; set; }

        [Column("dt_alteracao")]
        public DateTime? dataAlteracao { get; set; }

        [Column("id_user_cadastro")]
        public int? idUsuarioCadastro { get; set; }

        [Column("id_user_alteracao")]
        public int? idUsuarioAlteracao { get; set; }

    }
}
