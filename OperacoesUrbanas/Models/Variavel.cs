namespace OperacoesUrbanas.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ouc.tb_variavel_cepac")]
    public class Variavel
    {
        public Variavel()
        { }

        [Key]
        [Column("id_variavel")]
        public int id { get; set; }

        [Column("id_subsetor")]
        [ForeignKey("subSetor")]
        public int? idSubSetor { get; set; }
        public SubSetor subSetor { get; set; }

        [Column("id_categoria_uso")]
        [ForeignKey("categoriaUso")]
        public int? idCategoriaUso { get; set; }
        public CategoriaUso categoriaUso { get; set; }

        [StringLength(50)]
        [Column("nm_nome")]
        public string nome { get; set; }

        [Column("nr_valor")]
        public decimal valor { get; set; }

    }
}
