namespace OperacoesUrbanas.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ouc.tb_zona")]
    public class Zona
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Zona()
        {
            //tb_proposta = new HashSet<Proposta>();
            //tb_proposta1 = new HashSet<Proposta>();
        }

        [Key]
        [Column("id_zona")]
        public int id { get; set; }

        [StringLength(100)]
        [Column("nm_descricao")]
        public string nome { get; set; }
    }
}
