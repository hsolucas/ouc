namespace OperacoesUrbanas.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ouc.tb_subcategoria_uso")]
    public class SubCategoriaUso
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SubCategoriaUso()
        {
            //tb_proposta = new HashSet<Proposta>();
        }

        [Key]
        [Column("id_subcategoria_uso")]
        public int id { get; set; }

        [Column("id_categoria_uso")]
        [ForeignKey("categoriaUso")]
        public int? idCategoriaUso { get; set; }
        public CategoriaUso categoriaUso { get; set; }

        [StringLength(100)]
        [Column("nm_descricao")]
        public string nome { get; set; }

        public DateTime? dt_cadastro { get; set; }

        public DateTime? dt_alteracao { get; set; }

        public int? id_user_cadastro { get; set; }

        public int? id_user_alteracao { get; set; }

    }
}
