﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OperacoesUrbanas.Models
{
    [Table("ouc.tb_status_proposta")]
    public class Status
    {
        [Key]
        [Column("id_status_proposta")]
        public int id { get; set; }

        [Column("nm_descricao")]
        public string descricao { get; set; }

    }

    public enum StatusEnum
    {
        CALCULADA=1,
        TRANSMITIDA,
        EM_ANALISE,
        ENCERRADA,
        CANCELADA
    }


}