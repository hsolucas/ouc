﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OperacoesUrbanas.Models
{
    [Table("ouc.tb_usuario")]
    public class Usuario
    {

        public Usuario()
        {}

        [Key]
        [Column("id_usuario")]
        public int id { get; set; }

        [Display(Name = "Nome")]
        [Column("nm_nome")]
        public string nome { get; set; }

        [Display(Name = "E-Mail")]
        [Column("nm_email")]
        public string email { get; set; }

        [Display(Name = "Telefone")]
        [Column("nm_telefone")]
        public string telefone { get; set; }

        [Display(Name = "Senha")]
        [Column("nm_senha")]
        public string senha { get; set; }

    }
}