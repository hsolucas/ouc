﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OperacoesUrbanas.Models
{
    [Table("ouc.tb_interessado_proposta")]
    public class InteressadoProposta
    {
        [Key]
        [Column("id_interessado_proposta")]
        public int id { get; set; }

        [Column("id_proposta")]
        [ForeignKey("proposta")]
        public int? idProposta { get; set; }
        public Proposta proposta { get; set; }

        [Column("id_interessado")]
        [ForeignKey("pessoa")]
        public int? idInteressado { get; set; }
        public Pessoa pessoa { get; set; }
    }
}