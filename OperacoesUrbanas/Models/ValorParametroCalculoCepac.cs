namespace OperacoesUrbanas.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ouc.tb_valor_parametro_calc_cepac")]
    public class ValorParametroCalculoCepac
    {
        [Key]
        [Column("id_valor_parametro_calc_cepac")]
        public int id { get; set; }

        [Column("id_parametro_calc_cepac")]
        [ForeignKey("parametro")]
        public int? idParametro { get; set; }
        public ParametroCalculoCepac parametro { get; set; }

        [Column("id_proposta")]
        [ForeignKey("proposta")]
        public int? idProposta { get; set; }
        public Proposta proposta { get; set; }

        [Column("nr_valor")]
        public decimal? valor { get; set; }

        [NotMapped]
        public string nomeParametro { get; set; }

        public DateTime? dt_cadastro { get; set; }

        public DateTime? dt_alteracao { get; set; }

        public int? id_user_cadastro { get; set; }

        public int? id_user_alteracao { get; set; }
    }
}
