namespace OperacoesUrbanas.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ouc.tb_setor")]
    public class Setor
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Setor()
        {
            //tb_subsetor = new HashSet<SubSetor>();
        }

        [Key]
        [Column("id_setor")]
        public int id { get; set; }

        [Column("id_operacao_urbana")]
        [ForeignKey("operacaoUrbana")]
        public int? id_operacao_urbana { get; set; }
        [Display(Name ="OUC")]
        public OperacaoUrbana operacaoUrbana { get; set; }

        [StringLength(100)]
        [Column("nm_nome")]
        public string nome { get; set; }

        [Column("dt_cadastro")]
        public DateTime? dataCadastro { get; set; }

        [Column("dt_alteracao")]
        public DateTime? dataAlteracao { get; set; }

        [Column("id_user_cadastro")]
        public int? idUsuarioCadastro { get; set; }

        [Column("id_user_alteracao")]
        public int? idUsuarioAlteracao { get; set; }

    }
}
