namespace OperacoesUrbanas.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ouc.tb_parametro_calc_cepac")]
    public class ParametroCalculoCepac
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ParametroCalculoCepac()
        {
            //tb_valor_parametro_calc_cepac = new HashSet<ValorParametroCalculoCepac>();
        }

        [Key]
        [Column("id_parametro_calc_cepac")]
        public int id { get; set; }

        [StringLength(30)]
        [Column("nm_sigla")]
        public string sigla { get; set; }

        [StringLength(100)]
        [Column("nm_descricao")]
        public string descricao { get; set; }

        [Column("nr_valor_padrao")]
        public decimal? valorPadrao { get; set; }

        [Column("id_tipo_parametro_calc")]
        [ForeignKey("tipoParametroCalculo")]
        public int? idTipoParametro { get; set; }
        public TipoParametroCalculo tipoParametroCalculo { get; set; }

        public DateTime? dt_cadastro { get; set; }

        public DateTime? dt_alteracao { get; set; }

        public int? id_user_cadastro { get; set; }

        public int? id_user_alteracao { get; set; }

    }
}
