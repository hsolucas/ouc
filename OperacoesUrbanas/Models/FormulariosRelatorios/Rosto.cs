﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OperacoesUrbanas.Models.FormulariosRelatorios
{
    public struct Rosto
    {

        public string nomeOperacaoUrbana { get; set; }
        public string nomeSetor { get; set; }
        public string nomeSubSetor { get; set; }
        public string categoriaUso { get; set; }
        public string acca { get; set; }
        public string mupu { get; set; }
        public string aca_r { get; set; }
        public string aca_nr { get; set; }
        public string aumentoTaxaOcupacao { get; set; }
        public string aca { get; set; }
        public string qad { get; set; }
        public string qmp { get; set; }
        public string qto { get; set; }
        public string qt { get; set; }
        public string nomeUsuario { get; set; }
        public string rgUsuario { get; set; }
        public string proprietarioInteressado { get; set; }
        public string interessados { get; set; }
        public List<Pessoa> listaInteressados { get; set; }
        public string proprietarios { get; set; }
        public List<Pessoa> listaProprietarios { get; set; }
        public string contatos { get; set; }
        public List<Pessoa> listaContatos { get; set; }


    }
}