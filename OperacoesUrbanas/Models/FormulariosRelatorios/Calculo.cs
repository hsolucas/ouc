﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OperacoesUrbanas.Models.FormulariosRelatorios
{
    public struct Calculo
    {
        public int idOperacaoUrbana { get; set; }
        public string ato { get; set; }
        public string caBasico { get; set; }
        public string caProjeto { get; set; }
        public string armb { get; set; }
        public string atdc { get; set; }
        public string atdmv { get; set; }
        public string aca { get; set; }
        public string f1 { get; set; }
        public string f2 { get; set; }
        public string i1 { get; set; }
        public string i2 { get; set; }
        public string qad { get; set; }
        public string atrmp { get; set; }
        public string qmp { get; set; }
        public string qt { get; set; }
        public string atd { get; set; }
        public string ape { get; set; }
        public string aape { get; set; }
        public string f3 { get; set; }
        public string qto { get; set; }


    }
}