namespace OperacoesUrbanas.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ouc.tb_categoria_uso")]
    public class CategoriaUso
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CategoriaUso()
        {
        }

        [Key]
        [Column("id_categoria_uso")]
        public int id { get; set; }

        [StringLength(100)]
        [Column("nm_descricao")]
        public string nome { get; set; }

        [Column("dt_cadastro")]
        public DateTime? dataCadastro { get; set; }

        [Column("dt_alteracao")]
        public DateTime? dataAlteracao { get; set; }

        [Column("id_user_cadastro")]
        public int? IdUsuarioCadastro { get; set; }

        [Column("id_user_alteracao")]
        public int? IdUsuarioAlteracao { get; set; }

    }
}
