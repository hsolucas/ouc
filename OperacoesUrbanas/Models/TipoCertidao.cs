namespace OperacoesUrbanas.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ouc.tb_tipo_certidao")]
    public class TipoCertidao
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TipoCertidao()
        {
            //tb_proposta = new HashSet<Proposta>();
        }

        [Key]
        [Column("id_tipo_certidao")]
        public int id { get; set; }

        [StringLength(50)]
        [Column("nm_tipo_certidao")]
        public string nome { get; set; }

    }
}
