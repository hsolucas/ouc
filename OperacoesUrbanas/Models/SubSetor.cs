namespace OperacoesUrbanas.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ouc.tb_subsetor")]
    public class SubSetor
    {
        public SubSetor()
        {}

        [Key]
        [Column("id_subsetor")]
        public int id { get; set; }

        [Column("id_setor")]
        [ForeignKey("setor")]
        public int? idSetor { get; set; }
        public Setor setor { get; set; }

        [StringLength(100)]
        [Column("nm_nome")]
        public string nome { get; set; }

        [Column("dt_cadastro")]
        public DateTime? dataCadastro { get; set; }

        [Column("dt_alteracao")]
        public DateTime? dataAlteracao { get; set; }

        [Column("id_user_cadastro")]
        public int? idUsuarioCadastro { get; set; }

        [Column("id_user_alteracao")]
        public int? idUsuarioAlteracao { get; set; }

    }
}
