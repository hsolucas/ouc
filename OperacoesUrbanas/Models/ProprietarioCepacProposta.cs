﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OperacoesUrbanas.Models
{
    [Table("ouc.tb_proprietario_cepac_proposta")]
    public class ProprietarioCepacProposta
    {
        [Key]
        [Column("id_proprietario_cepac_proposta")]
        public int id { get; set; }

        [Column("id_proposta")]
        [ForeignKey("proposta")]
        public int? idProposta { get; set; }
        public Proposta proposta { get; set; }

        [Column("id_proprietario")]
        [ForeignKey("pessoa")]
        public int? idProprietario { get; set; }
        public Pessoa pessoa { get; set; }
    }
}