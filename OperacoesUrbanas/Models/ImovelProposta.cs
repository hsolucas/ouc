namespace OperacoesUrbanas.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ouc.tb_imovel_proposta")]
    public class ImovelProposta
    {
        [Key]
        [Column("id_imovel_proposta")]
        public int id { get; set; }

        [Column("id_proposta")]
        [ForeignKey("proposta")]
        public int? idProposta { get; set; }
        public Proposta proposta { get; set; }

        [Column("id_imovel")]
        [ForeignKey("imovel")]
        public int? idImovel { get; set; }
        public Imovel imovel { get; set; }

    }
}
