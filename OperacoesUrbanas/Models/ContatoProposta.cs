﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OperacoesUrbanas.Models
{
    [Table("ouc.tb_contato_proposta")]
    public class ContatoProposta
    {
        [Key]
        [Column("id_contato_proposta")]
        public int id { get; set; }

        [Column("id_proposta")]
        [ForeignKey("proposta")]
        public int? idProposta { get; set; }
        public Proposta proposta { get; set; }

        [Column("id_contato")]
        [ForeignKey("pessoa")]
        public int? idContato { get; set; }
        public Pessoa pessoa { get; set; }
    }
}