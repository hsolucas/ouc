namespace OperacoesUrbanas.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ouc.tb_proposta")]
    public class Proposta
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Proposta()
        {
            //tb_imovel_proposta = new HashSet<ImovelProposta>();
            //tb_valor_parametro_calc_cepac = new HashSet<ValorParametroCalculoCepac>();
        }

        [Key]
        [Display(Name = "ID")]
        [Column("id_proposta")]
        public int id { get; set; }

        [Column("id_tipo_certidao")]
        [ForeignKey("tipoCertidao")]
        public int idTipoCertidao { get; set; }
        public TipoCertidao tipoCertidao { get; set; }

        [Column("id_tipo_pedido")]
        [ForeignKey("tipoPedido")]
        public int idTipoPedido { get; set; }
        public TipoPedido tipoPedido { get; set; }

        [Display(Name = "Sub-Setor")]
        [Column("id_subsetor")]
        [ForeignKey("subSetor")]
        public int idSubSetor { get; set; }
        public SubSetor subSetor { get; set; }

        [Display(Name = "Zona Atual")]
        [Column("id_zona_atual")]
        [ForeignKey("zonaAtual")]
        public int? idZonaAtual { get; set; }
        public Zona zonaAtual { get; set; }

        [Column("id_zona_anterior")]
        [ForeignKey("zonaAnterior")]
        public int? idZonaAnterior { get; set; }
        public Zona zonaAnterior { get; set; }

        [Column("id_subcategoria_uso")]
        [ForeignKey("subCategoriaUso")]
        public int? idSubCategoriaUso { get; set; }
        public SubCategoriaUso subCategoriaUso { get; set; }

        [Column("fl_acca")]
        public int? _acca { get; set; }
        [NotMapped]
        public bool acca
        {
            get
            {
                return _acca == 1;
            }
            set
            {
                _acca = value ? 1 : 0;
            }
        }

        [Column("id_pessoa_interessada")]
        [ForeignKey("interessado")]
        public int? idInteressado { get; set; }
        public Pessoa interessado { get; set; }

        [Column("id_pessoa_proprietaria_cepac")]
        [ForeignKey("proprietarioCepac")]
        public int? idProprietarioCepac { get; set; }
        public Pessoa proprietarioCepac { get; set; }

        [Column("id_pessoa_contato")]
        [ForeignKey("contato")]
        public int? idContato { get; set; }
        public Pessoa contato { get; set; }

        [Column("fl_pedido_alvara")]
        public int? _pedidoAlvara { get; set; }
        [NotMapped]
        public bool pedidoAlvara
        {
            get
            {
                return _pedidoAlvara == 1;
            }
            set
            {
                _pedidoAlvara = value ? 1 : 0;
            }
        }

        [Display(Name = "Taxa Ocupa��o M�x")]
        [Column("nr_taxa_ocupacao_max")]
        public decimal? taxaOcupacaoMaxima { get; set; }

        [Display(Name = "Gabarito")]
        [Column("nr_gabarito")]
        public decimal? gabarito { get; set; }

        [Display(Name = "CA B�sico")]
        [Column("nr_ca_basico")]
        public decimal? caBasico { get; set; }

        [Display(Name = "CA Projeto")]
        [Column("nr_ca_projeto")]
        public decimal? caProjeto { get; set; }

        [Display(Name = "�rea Real Total")]
        [Column("nr_area_real_total")]
        public decimal? areaRealTotal { get; set; }

        [Display(Name = "�rea Escritura Total")]
        [Column("nr_area_escritura_total")]
        public decimal? areaEscrituraTotal { get; set; }

        [Column("fl_desapropriacao")]
        public int? _desapropriacao { get; set; }
        [NotMapped]
        public bool desapropriacao
        {
            get
            {
                return _desapropriacao == 1;
            }
            set
            {
                _desapropriacao = value ? 1 : 0;
            }
        }

        [Column("fl_potencial_adicional")]
        public int? _potencialAdicional { get; set; }
        [NotMapped]
        public bool potencialAdicional
        {
            get
            {
                return _potencialAdicional == 1;
            }
            set
            {
                _potencialAdicional = value ? 1 : 0;
            }
        }

        [Display(Name = "CEPACs")]
        [Column("nr_qtde_cepac")]
        public int? qtdeCepac { get; set; }

        [Column("nr_calculo_acca")]
        public int? calculoAcca { get; set; }

        [Column("id_status_proposta")]
        [ForeignKey("status")]
        public int? idStatus { get; set; }
        [Display(Name = "Status")]
        public Status status { get; set; }

        [Display(Name = "Cadastro")]
        public DateTime? dt_cadastro { get; set; }

        [Display(Name = "Altera��o")]
        public DateTime? dt_alteracao { get; set; }

        [Display(Name = "Usu�rio")]
        public int? id_user_cadastro { get; set; }

        public int? id_user_alteracao { get; set; }

        [NotMapped]
        public List<Imovel> imoveis { get; set; }

        [NotMapped]
        public List<Pessoa> interessados { get; set; }

        [NotMapped]
        public List<Pessoa> proprietarios { get; set; }

        [NotMapped]
        public List<Pessoa> contatos { get; set; }

        [NotMapped]
        public ValorParametroCalculoCepac[] valoresParametros { get; set; }

    }
}
