namespace OperacoesUrbanas.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ouc.tb_operacao_urbana")]
    public class OperacaoUrbana
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OperacaoUrbana()
        {
            //tb_parametro_calc_cepac = new HashSet<ParametroCalculoCepac>();
            //tb_setor = new HashSet<Setor>();
        }

        [Key]
        [Column("id_operacao_urbana")]
        public int id { get; set; }

        [StringLength(50)]
        [Column("nm_nome")]
        public string nome { get; set; }

        [StringLength(50)]
        [Column("nm_lei")]
        public string lei { get; set; }

        [Column("nr_area_max")]
        public decimal? areaMaxima { get; set; }

        [Column("nr_area_vendida")]
        public decimal? areaVendida { get; set; }

        [Column("nr_valor_arrecadado")]
        public decimal? valorArrecadado { get; set; }

        [Column("nr_valor_gasto")]
        public decimal? valorInvestido { get; set; }

        [Column("nr_juros_aplicacao")]
        public decimal? juros { get; set; }

        [Column("dt_cadastro")]
        public DateTime? dataCadastro { get; set; }

        [Column("dt_alteracao")]
        public DateTime? dataAlteracao { get; set; }

        [Column("id_user_cadastro")]
        public int? idUsuarioCadastro { get; set; }

        [Column("id_user_alteracao")]
        public int? idUsuarioAlteracao { get; set; }

    }
}
