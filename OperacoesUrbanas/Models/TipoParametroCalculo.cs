namespace OperacoesUrbanas.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ouc.tb_tipo_parametro_calc")]
    public class TipoParametroCalculo
    {
        [Key]
        [Column("id_tipo_parametro_calc")]
        public int id { get; set; }

        [StringLength(50)]
        [Column("nm_tipo_parametro_calc")]
        public string nome { get; set; }
    }
}
