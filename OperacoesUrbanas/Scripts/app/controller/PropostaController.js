﻿var idElOpUrbana = '#operacaoUrbana';
var idElSetor = '#setor';
var idElSubSetor = '#subSetor';
var idSubSetor = null;
var idElCategoria = '#categoriaUso';
var idElSubCategoria = '#subCategoriaUso';
var idElAddImovel = '#btnAddImovel';
var idElFinalizarImovel = '#btnFinalizarImovel';

$(window).load(function () {
    $('.loader').fadeToggle('slow');
});

$(function () {

    getTiposCertidao();
    getOperacoesUrbanas();
    getTiposPedido();
    getZonas();
    getCategorias();
    /*
    $(idElSetor).change(function () {
        var text = $('#setor :selected') ? $('#setor :selected').text() : '';
        $('input[name=nomeSetor').val(val);
        getSubSetores($(this).val());
    });
    
    $(idElSubSetor).change(function () {
        var val = $(idElSubSetor + ' :selected') ? $(idElSubSetor + ' :selected').text() : '';
        $('input[name=nomeSubSetor').val(val);
        var idSubSetor = $(idElSubSetor).val();
        proposta.calculoOpUrbana.carregarVariaveisFixas(idSubSetor);
    });
    */

    $(idElCategoria).change(function () {
        if (!idSubSetor || idSubSetor == null || idSubSetor == '') {
            base.methods.showMessage('É obrigatório selecionar o sub-setor.');
            return;
        }
        var idCategoria = $(idElCategoria).val();
        getSubCategorias(idCategoria);
        proposta.calculoOpUrbana.carregarVariaveisFixas(idSubSetor, idCategoria);
    });

    $('input[name=pedidoAlvara]').change(function () {
        if ($(this).val() == '1') {
            $('#div-ato-original').fadeOut();
            $('#div-desapropriacao').show(500);
        } else {
            $('#div-ato-original').show(500);
            $('#div-desapropriacao').fadeOut();
        }
        proposta.atualizarArea();
    });

    $('input[name=doacaoDesapropriacao]').change(function () {
        if ($(this).val() == '1') {
            $('#div-sem-doacao').fadeOut();
            $('#div-ato-acca').show(500);
        } else {
            $('#div-sem-doacao').show(500);
            $('#div-ato-acca').fadeOut();
        }
        proposta.atualizarArea();
    });

    $(idElAddImovel).click(function () {
        if (proposta != null) {
            proposta.addImovel();
            $(idElFinalizarImovel).prop('disabled', false);
        } else {
            alert('Favor completar as informações acima.');
        }

    });

    $('#btnAddInteressado').click(function () {
        if (proposta != null) {
            proposta.addInteressado();
        }
    });

    $('#btnAddProprietario').click(function () {
        if (proposta != null) {
            proposta.addProprietario();
        }
    });

    $('#btnAddContato').click(function () {
        if (proposta != null) {
            proposta.addContato();
        }
    });

    $(idElFinalizarImovel).click(function () {
        proposta.atualizarArea();
    });

    $('.parametro-leitura').blur(function () {
        proposta.atualizarCalculo();
    });

    $('.parametro-leitura-area').blur(function () {
        proposta.atualizarArea();
    });

    $('#btn-simular').click(function () {
        if (validarPreenchimento()) {
            proposta.simular();
        } else {
            base.methods.showMessage('Favor preencher todas as informações requeridas.');
        }
    });

    $('#btn-validar').click(function () {
        if (validarPreenchimento()) {
            proposta.finalizar();
        } else {
            base.methods.showMessage('Favor preencher todas as informações requeridas.');
        }
    });

    $('#btn-encerrar').click(function () {
        if($('#deAcordo').is(':checked')){
            proposta.encerrar();
        } else {
            base.methods.showMessage('É necessário confirmar que está de acordo com a declaração de responsabilidade.');
        }
    });

    $('#lnk-frm-rosto').click(function () {
        proposta.imprimirFormRosto();
    });

    $('#proprietarioInteressado').change(function () {
        var val = ($('#proprietarioInteressado').is(':checked')) ? '1' : '0';
        $('input[name=proprietarioInteressado]').val(val);
        $('#divProprietarios, #divAddProprietarios').slideToggle(500);
    });

    //on() para elementos que sao gerados dinamicamente
    $('#divImoveis').on('keyup', 'input', function () {
        var id = $(this).attr('id');
        var mask = id.indexOf("Contribuinte") > 0 ? 'sql' : 'money';
        var inputMask = getMaskField(mask);
        if (inputMask != null) {
            maskIt(this, event, inputMask.mask, true);
        }
    });

    //PFXPJ
    $('#divImoveisCompl').on('change', 'input', function () {
        var name = $(this).attr('name');
        if (name && name.indexOf('proprietario-imovel') >= 0) {
            var val = $(this).val();
            try{
                var nameSpl = name.split('proprietario-imovel');
                var index = nameSpl[1];
                if(val == '1'){
                    $('#div-cnpj'+index).hide();
                    $('#div-cpf'+index).show();
                }else if(val == '2'){
                    $('#div-cnpj'+index).show();
                    $('#div-cpf'+index).hide();
                }
            } catch (err) {
                console.log(err);
            }
        }
    });

    $('#divImoveisCompl').on('keyup', 'input', function () {
        var id = $(this).attr('id');
        var mask = "";
        if(id.indexOf("cpf") > 0){
            mask = "cpf";
        }else if(id.indexOf("cnpj") > 0){
            mask="cnpj";
        }else{
            return;
        }
        var inputMask = getMaskField(mask);
        if (inputMask != null) {
            maskIt(this, event, inputMask.mask, true);
        }
    });

    $('#divOperacaoUrbana').on('change', 'input', function () {
        var idOpUrbana = $(this).val();
        getSetores(idOpUrbana);
        loadCalculo(idOpUrbana);
        var name = $(this).attr('name');
        if (name && name.indexOf('operacaoUrbana') >= 0) {
            var val = $(this).parent().text();
            $('input[name=nomeOperacaoUrbana]').val(val);
        }
    });

    $('#divSetor').on('change', 'input', function () {
        $('input[name=nomeSetor]').val($(this).text());
        getSubSetores($(this).val());
    });

    $('#divSubSetor').on('change', 'input', function () {
        $('input[name=nomeSubSetor]').val($(this).text());
        idSubSetor = $(this).val();
        //proposta.calculoOpUrbana.carregarVariaveisFixas($(this).val(), null);
    });




    $('#divInteressados').on('change', 'input', function () {
        var name = $(this).attr('name');
        if (name && name.indexOf('tipo-interessado') >= 0) {
            var val = $(this).val();
            try {
                var nameSpl = name.split('tipo-interessado');
                var index = nameSpl[1];
                if (val == '1') {
                    $('#div-cnpj-interessado-pj' + index).hide();
                    $('#div-cpf-interessado-pf' + index).show();
                } else if (val == '2') {
                    $('#div-cnpj-interessado-pj' + index).show();
                    $('#div-cpf-interessado-pf' + index).hide();
                }
            } catch (err) {
                console.log(err);
            }
        }
    });

    $('#divProprietarios').on('change', 'input', function () {
        var name = $(this).attr('name');
        if (name && name.indexOf('tipo-proprietario') >= 0) {
            var val = $(this).val();
            try {
                var nameSpl = name.split('tipo-proprietario');
                var index = nameSpl[1];
                if (val == '1') {
                    $('#div-cnpj-proprietario-pj' + index).hide();
                    $('#div-cpf-proprietario-pf' + index).show();
                } else if (val == '2') {
                    $('#div-cnpj-proprietario-pj' + index).show();
                    $('#div-cpf-proprietario-pf' + index).hide();
                }
            } catch (err) {
                console.log(err);
            }
        }
    });

});

function getTiposCertidao() {
    var htmlTipos = new html('#divTipoCertidao', '', '', 'tipoCertidao', 'tipoCertidao', null);
    base.methods.getAll(htmlTipos, 'TiposCertidao');

}

function getTiposPedido() {
    var htmlTipos = new html('#divTipoPedido', '', '', 'tipoPedido', 'tipoPedido', null);
    base.methods.getAll(htmlTipos, 'TiposPedido');

}

function getZonas() {
    var htmlZonaAtual = new html('#zonaAtual', '', '', 'zonaAtual', 'zonaAtual', 0);
    base.methods.getAll(htmlZonaAtual, 'Zonas');
}

function getOperacoesUrbanas() {
    var htmlOperacoes = new html('#divOperacaoUrbana', '', '', 'operacaoUrbana', 'operacaoUrbana', null);
    base.methods.getAll(htmlOperacoes, 'OpUrbanas');

}

function getCategorias() {
    $(idElCategoria).html('<option value="">- Selecionar -</option>');
    $(idElSubCategoria).html('');
    var htmlCategorias = new html(idElCategoria, '', '', 'categoriaUso', 'categoriaUso', 0);
    base.methods.getAll(htmlCategorias, 'CategoriasUso');
    htmlCategorias = new html('#divCategoriaUsoAcca', '', '', 'categoriaUsoAcca', 'categoriaUsoAcca', null);
    base.methods.getAll(htmlCategorias, 'CategoriasUso');
}

function getSubCategorias(idCategoria) {
    $(idElSubCategoria).html('<option value="">- Selecionar -</option>');
    var htmlCategorias = new html(idElSubCategoria, '', '', 'subCategoriaUso', 'subCategoriaUso', 0);
    base.methods.getAll(htmlCategorias, 'subcategoriasuso/categoriauso/' + idCategoria);
}

function getSetores(idOpUrbana) {
    //$(idElSetor).html('<option value="">- Selecionar -</option>');
    //$(idElSubSetor).html('');
    //var htmlSetores = new html(idElSetor, '', '', 'setor', 'setor', 0);
    $('#label-setor').show();
    $('#divSetor').html('');
    $('#divSubSetor').html('');
    var htmlSetores = new html('#divSetor', '', '', 'setor', 'setor', null);
    base.methods.getAll(htmlSetores, 'Setores/OpUrbana/' + idOpUrbana);
}

function getSubSetores(idSetor) {
    //$(idElSubSetor).html('<option value="">- Selecionar -</option>');
    //var htmlSetores = new html(idElSubSetor, '', '', 'subSetor', 'subSetor', 0);
    $('#label-sub-setor').show();
    $('#divSubSetor').html('');
    var htmlSetores = new html('#divSubSetor', '', '', 'subSetor', 'subSetor', null);
    base.methods.getAll(htmlSetores, 'subsetores/setor/' + idSetor);
}

function loadCalculo(idOpUrbana) {
    $('div[id^="calculo-op"]').hide();
    $('#calculo-op-' + idOpUrbana).show();
    var calculo = (idOpUrbana == 1) ? new CalculoAguaEspraiada() : new CalculoFariaLima();
    proposta = new Proposta(calculo);
    $('#botoes-conclusao').show();
}

function validarPreenchimento() {
    var valido = true;
    $('input[required], select[required]').each(function () {
        var val = $(this).val();
        if (val == null || val == '') {
            valido = false;
            return false;
        }
    });
    if (valido === true) {
        $('#areaEscrituraTotal, #areaRealTotal').each(function () {
            var val = $(this).val();
            if (val == null || val == '' || parseFloat(val) < 1) {
                valido = false;
                return false;
            }
        });
    }
    return valido;
}