﻿var serverUrl = 'http://localhost:53461/';
var apiUrl = serverUrl + 'api/';

var base = {
    methods: {
        /*
            Listar todos
        */
        getAll: function (htmlObject, apiMethod) {
            var htmlElement = htmlObject;
            var url = apiUrl + apiMethod;
            $.get(url)
                .then(function (resp) {
                    if (resp == null || resp == "" || resp.length < 1) {
                        console.log("Resposta sem dados " + url);
                    } else {
                        for (var i = 0; i < resp.length; i++) {
                            var e = resp[i];
                            htmlElement.text = e.nome ? e.nome : '';
                            htmlElement.value = e.id ? e.id : '';
                            htmlElement.id = e.id ? e.id : '';
                            (htmlElement.index != null) ? htmlElement.addSelectOption() : htmlElement.addRadioOption();
                        }
                    }
                })
                .fail(function (resp) {
                    console.log("Erro ao obter resposta " + url);
                    console.log(resp.responseText);
                });
        },

        /*
            Obter pelo id
        */
        getById: function () { },

        currencyToFloat: function (currencyValue) {
            var tmp = currencyValue.replace('.', '');
            tmp = tmp.replace(',', '.');
            var floatValue = parseFloat(tmp);
            return isNaN(floatValue) ? null : floatValue;
        },

        floatToCurrency: function (floatValue) {
            var valueAsStr = '' + floatValue;
            var tmp = valueAsStr.split('.');
            var intPart = tmp[0];
            var decimalPart = tmp.length > 1 ? ',' + tmp[1] : ',00';
            if (decimalPart.length == 2) {
                decimalPart += '0';
            }
            else if (decimalPart.length > 3) {
                decimalPart = decimalPart.substr(0, 3);
            }

            var regex = /(\d+)(\d{3})/;
            while (regex.test(intPart)) {
                intPart = intPart.replace(regex, '$1' + '.' + '$2');
            }
            return intPart + decimalPart;
        },

        showMessage: function (message) {
            $('#msg-usuario').text(message);
            $('#dlg-mensagem').modal('toggle');
        }

    }
}