﻿function html(elementSelector) {
    html(elementSelector, '', '', '', '', '');
}


function html(elementSelector, text, value, id, name, index) {

    this.text = text;
    this.elementSelector = elementSelector;
    this.value = value;
    this.id = id;
    this.name = name;
    this.index = index;

    this.getElementFloatVal = function (decimais) {
        var el = $(this.elementSelector);
        var val = 0;
        if (el && el.val()) {
            var content = el.val();
            content = content.replace(/\./g, '');
            content = content.replace(/,/g, '.');
            if (!isNaN(content)) {
                var floatNumber = parseFloat(content);
                return parseFloat(floatNumber.toFixed(decimais));
            }
        }
        return 0;
    }

    this.addSelectOption = function () {
        var el = $(this.elementSelector);
        if (el) {
            var atualHTML = el.html();
            var optionInnerHTML = '<option value="'+this.value+'">'+this.text+'</option>';
            el.html(atualHTML + optionInnerHTML);
        }
    }

    this.addRadioOption = function () {
        var el = $(this.elementSelector);
        if (el) {
            var atualHTML = el.html();
            var optionInnerHTML = '<div class="radio"><label><input type="radio" name="' + this.name + '" id="' + this.name + this.id + '" value="' + this.value + '" >' + this.text + '</label></div>';
            el.html(atualHTML + optionInnerHTML);
        }
    }

    this.addBootstrapRadioOption = function(){
        var el = $(this.elementSelector);
        if (el) {
            var atualHTML = el.html();
            var optionInnerHTML = '<label class="btn btn-default btn-toggle-radio col-md-6 col-md-offset-12"><input type="radio" name="' + this.name +'" id="' + this.name  + this.id + '" value="'+ this.value +'" autocomplete="off">'+ this.text +'</label>';
            el.html(atualHTML + optionInnerHTML);
        }
    }

    this.addInputtext = function (disabled) {
        var el = $(this.elementSelector);
        if (el) {
            var atualHtml = el.html();
            var htmlLabel = '<div class="form-group" id="div-' + this.id +'"><label for="' + this.id + '" class="col-md-2 control-label">' + this.text + '</label>';
            var htmlInputText = '<div class="col-md-4"><input type="text" id="' + this.id + '" value="' + this.value + '" class="form-control" ';
            if (disabled && disabled === true) {
                htmlInputText += 'readonly';
            }
            htmlInputText += '/></div></div>';
            el.html(atualHtml + htmlLabel + htmlInputText);
            //el.after(htmlLabel + htmlInputText);
        }
    }

    this.append = function (htmlToAppend) {
        var el = $(this.elementSelector);
        if (el) {
            var atualHtml = el.html();
            el.html(atualHtml + htmlToAppend);
        }
    }

}
/*
var html = {

    elementSelector: null,
    text: null,
    value: null,
    id: null,
    name: null,
    index: null,

    addSelectOption : function(){
        var el = $(this.elementSelector);
        if (el) {
            var option = document.createElement("option");
            option.text = this.text;
            option.value = this.value;
            x.add(option, (this.index != null ? this.index : 0));
        }
    },

    addBootstrapRadioOption : function(){
        var el = $(this.elementSelector);
        if (el) {
            var atualHTML = el.html();
            var optionInnerHTML = '<label class="btn btn-default btn-toggle-radio col-md-6 col-md-offset-12"><input type="radio" name="' + this.name +'" id="' + this.id + '" value="'+ this.value +'" autocomplete="off">'+ this.text +'</label>';
            el.html(atualHTML + optionInnerHTML);
        }
    }

};
*/