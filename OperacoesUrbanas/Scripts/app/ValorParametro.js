﻿function gravarValores(valoresParametros, indiceInicial) {
    if (!valoresParametros.length || valoresParametros.length < 1) {
        console.log("Array de objetos valorParametro requerido.");
        return;
    }
    if (indiceInicial >= valoresParametros.length) {
        console.log("Fim do Array de objetos.");
        return;
    }
    var url = apiUrl + 'valores/gravar';
    var postdata = valoresParametros[indiceInicial];

    $.post(url, postdata)
        .then(function (resp) {
            console.log("Valor gravado!" + postdata.nomeParametro + " " + resp);
            gravarValores(valoresParametros, ++indiceInicial);
        })
        .fail(function (resp) {
            var msg = "Erro ao gravar valor : " + postdata.nomeParametro + " " + valor + " " + resp.status + " " + resp.statusText;
            console.log(msg);
            if (indiceInicial > 0) {
                removerValoresProposta(postdata.idProposta);
            }
            throw msg;
        })

}


function gravarValor(valor, nomeParametro, idProposta) {
    if (idProposta != null && nomeParametro != null) {

        var url = apiUrl + 'valores/gravar';
        var postdata = {idProposta : idProposta, nomeParametro : nomeParametro, valor: valor};

        $.post(url, postdata)
            .then(function (resp) {
                console.log("Valor gravado!" + resp);
            })
            .fail(function (resp) {
                removerValoresProposta(idProposta);
                var msg = "Erro ao gravar valor : " + nomeParametro + " " + valor + " " + resp.status + " " + resp.statusText;
                console.log(msg);
            })
    }
    else {
        throw "Faltou informação";
    }
}

function removerValoresProposta(idProposta) {
    var url = apiUrl + 'valores/remover';
    var postdata = { idProposta: idProposta };

    $.post(url, postdata)
        .then(function (resp) {
            console.log("Valores da proposta removidos! " + resp);
        })
        .fail(function (resp) {
            console.log("Valores não removidos... " + resp.status + " " + resp.statusText);
            //throw "Valores não removidos... " + resp.status + " " + resp.statusText;
        })
}