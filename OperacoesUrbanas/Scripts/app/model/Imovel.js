﻿
function imovel(id, idProprietario, areaReal, areaEscritura, matricula, endereco, contribuinte) {
    this.id = id;
    this.idPropietario = idProprietario;
    this.areaReal = areaReal;
    this.areaEscritura = areaEscritura;
    this.matricula = matricula;
    this.endereco = endereco;
    this.contribuinte = contribuinte;
    this.proprietario = null;
    this.addImovel = function (index) {
        addFormImovel(this, index);
    }
    this.addImovelCompl = function (index) {
        addFormComplementoImovel(this, index);
    }
    this.salvar = function (proposta) {
        salvar(this, proposta);
    }

}

function imovel() {
    this.id = 0;
    this.idPropietario = null;
    this.areaReal = 0;
    this.areaEscritura = 0;
    this.matricula = '';
    this.endereco = '';
    this.contribuinte = '';
    this.proprietario = null;
    this.addImovel = function (index) {
        addFormImovel(this, index);
    }
    this.addImovelCompl = function (index) {
        addFormComplementoImovel(this, index);
    }
    this.salvar = function (idProposta) {
        salvar(this, idProposta);
    }
}

function addFormImovel(imovel, index) {
    try {
        var idDivImovel = 'imovel' + index;
        var htmlImovel = new html('#divImoveis', '', '', '', '', '');
        htmlImovel.append('<h5>Imóvel ' + index + '</h5><div id="' + idDivImovel + '"></div>');

        var idElContribuinte = 'codContribuinte' + index;
        var htmlContribuinte = new html('#' + idDivImovel, 'Contribuinte/SQL', imovel.contribuinte, idElContribuinte, idElContribuinte, null);
        htmlContribuinte.addInputtext();

        var htmlAreaReal = new html('#' + idDivImovel, 'Área Real', imovel.areaReal, 'areaReal' + index, 'areaReal' + index, null);
        htmlAreaReal.addInputtext();

        var htmlAreaEscritura = new html('#' + idDivImovel, 'Área Escritura', imovel.areaEscritura, 'areaEscritura' + index, 'areaEscritura' + index, null);
        htmlAreaEscritura.addInputtext();

        //document.getElementById('"' + idElContribuinte + '"').onblur = function () { getImovelBySql(this.value); };
    }catch(ex){
        console.log("Erro ao adicionar imóvel " + ex);
    }

}

function addFormComplementoImovel(imovel, index) {
    index = index + 1;
    var idDivImovel = 'imovelCompl' + index;
    try {
        var htmlImovel = new html('#divImoveisCompl', '', '', '', '', '');
        htmlImovel.append('<h5>Imóvel ' + index + '</h5><div id="' + idDivImovel + '"></div>');

        var idElId = 'idCompl' + index;
        var htmlId = new html('#' + idDivImovel, 'ID', imovel.id, idElId, idElId, null);
        htmlId.addInputtext(true);

        var idElContribuinte = 'codContribuinteCompl' + index;
        var htmlContribuinte = new html('#' + idDivImovel, 'Contribuinte/SQL', imovel.contribuinte, idElContribuinte, idElContribuinte, null);
        htmlContribuinte.addInputtext(true);

        var htmlAreaReal = new html('#' + idDivImovel, 'Área Real', base.methods.floatToCurrency(imovel.areaReal), 'areaRealCompl' + index, 'areaRealCompl' + index, null);
        htmlAreaReal.addInputtext(true);

        var htmlAreaEscritura = new html('#' + idDivImovel, 'Área Escritura', base.methods.floatToCurrency(imovel.areaEscritura), 'areaEscrituraCompl' + index, 'areaEscrituraCompl' + index, null);
        htmlAreaEscritura.addInputtext(true);

        var htmlMatricula = new html('#' + idDivImovel, 'Matrícula', imovel.matricula, 'matriculaCompl' + index, 'matriculaCompl' + index, null);
        htmlMatricula.addInputtext();

        var htmlEndereco = new html('#' + idDivImovel, 'Endereço', imovel.endereco, 'enderecoCompl' + index, 'enderecoCompl' + index, null);
        htmlEndereco.addInputtext();

        imovel.proprietario = new Pessoa();
        var codigoHtmlProprietario = '<div class="form-group"><label class="col-md-12 col-md-pull-8 control-label">Proprietário</label>';
        codigoHtmlProprietario += '<div class="radio col-md-2 col-md-offset-1"><label><input type="radio" name="proprietario-imovel' + index + '" id="fisica" value="1">Pessoa Física</label></div>';
        codigoHtmlProprietario += '<div class="radio col-md-2"><label><input type="radio" name="proprietario-imovel' + index + '" id="juridica" value="2">Pessoa Jurídica</label></div></div>';
        var htmlProprietario = new html('#' + idDivImovel);
        htmlProprietario.append(codigoHtmlProprietario);

        var htmlCnpj = new html('#' + idDivImovel, 'CNPJ', imovel.proprietario.cnpj, 'cnpj' + index, 'cnpj' + index, null);
        htmlCnpj.addInputtext();

        var htmlCpf = new html('#' + idDivImovel, 'CPF', imovel.proprietario.cpf, 'cpf' + index, 'cpf' + index, null);
        htmlCpf.addInputtext();

        var htmlNome = new html('#' + idDivImovel, 'Nome', imovel.proprietario.nome, 'nome' + index, 'nome' + index, null);
        htmlNome.addInputtext();

    } catch (ex) {
        console.log("Erro ao adicionar imóvel " + ex);
    }

}

function salvar(imovel, idProposta) {
    var url = apiUrl + 'imoveis/salvar';
    $.post(url, imovel)
        .then(function (imovelSalvo) {
            if (idProposta != null) {
                var imovelProposta = {};
                imovelProposta.idImovel = imovelSalvo.id;
                imovelProposta.idProposta = idProposta;
                try {
                    salvarImovelProposta(imovelProposta);
                } catch (err) {
                    throw err;
                }
            }
        })
        .fail(function (resp) {
            alert('Erro ao salvar imóvel! ' + resp.status + ' ' + resp.statusText);
        });
}

function salvarImovelProposta(imovelProposta) {
    var url = apiUrl + 'imoveis/proposta/salvar';
    $.post(url, imovelProposta)
        .then(function (imovelPropostaSalvo) {
            console.log("Imovel Proposta OK " + imovelPropostaSalvo.id);
        })
        .fail(function (resp) {
            throw 'Erro ao salvar imóvel proposta! ' + resp.status + ' ' + resp.statusText;
        });
}

function getImovelBySql(sql) {
    alert(sql);
}