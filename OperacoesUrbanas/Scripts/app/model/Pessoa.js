﻿function Pessoa() {
    this.idClassificacaoPessoa = 1;
    this.nome = '';
    this.cpf = '';
    this.cnpj = '';
    this.endereco = '';
    this.rg = '';
    this.telefone = '';
    this.telefone2 = '';
    this.email = '';
    this.profissao = '';
    this.nacionalidade = '';

    this.addInteressadoCepac = function (index) {
        try {
            var idDivInteressados = 'interessado' + index;
            var htmlImovel = new html('#divInteressados', '', '', '', '', '');
            htmlImovel.append('<h5>Interessado ' + index + '</h5><div id="' + idDivInteressados + '"></div>');

            var codigoHtmlProprietario = '<div class="form-group"><label class="col-md-12 col-md-pull-8 control-label">Tipo</label>';
            codigoHtmlProprietario += '<div class="radio col-md-2 col-md-offset-1"><label><input type="radio" name="tipo-interessado' + index + '" id="tipo-interessado-fisica" value="1">Pessoa Física</label></div>';
            codigoHtmlProprietario += '<div class="radio col-md-2"><label><input type="radio" name="tipo-interessado' + index + '" id="tipo-interessado-juridica" value="2">Pessoa Jurídica</label></div></div>';
            var htmlProprietario = new html('#' + idDivInteressados);
            htmlProprietario.append(codigoHtmlProprietario);

            var htmlNome = new html('#' + idDivInteressados, 'Nome', this.nome, 'nome-interessado' + index, 'nome-interessado' + index, null);
            htmlNome.addInputtext();

            var htmlRg = new html('#' + idDivInteressados, 'RG', this.rg, 'rg-interessado' + index, 'rg-interessado' + index, null);
            htmlRg.addInputtext();

            var htmlCpf = new html('#' + idDivInteressados, 'CPF', this.cpf, 'cpf-interessado-pf' + index, 'cpf-interessado-pf' + index, null);
            htmlCpf.addInputtext();

            var htmlCnpj = new html('#' + idDivInteressados, 'CNPJ', this.cnpj, 'cnpj-interessado-pj' + index, 'cnpj-interessado-pj' + index, null);
            htmlCnpj.addInputtext();

            var htmlEndereco = new html('#' + idDivInteressados, 'Endereço', this.endereco, 'endereco-interessado' + index, 'endereco-interessado' + index, null);
            htmlEndereco.addInputtext();

            var htmlNacionalidade = new html('#' + idDivInteressados, 'Nacionalidade', this.nacionalidade, 'nacionalidade-interessado' + index, 'nacionalidade-interessado' + index, null);
            htmlNacionalidade.addInputtext();

            var htmlProfissao = new html('#' + idDivInteressados, 'Profissão', this.profissao, 'profissao-interessado' + index, 'profissao-interessado' + index, null);
            htmlProfissao.addInputtext();

        } catch (ex) {
            console.log("Erro ao adicionar interessado " + ex);
        }
    }

    this.addProprietarioCepac = function (index) {
        try {
            var idDivProprietarios = 'proprietario' + index;
            var htmlImovel = new html('#divProprietarios', '', '', '', '', '');
            htmlImovel.append('<h5>Proprietário ' + index + '</h5><div id="' + idDivProprietarios + '"></div>');

            var codigoHtmlProprietario = '<div class="form-group"><label class="col-md-12 col-md-pull-8 control-label">Tipo</label>';
            codigoHtmlProprietario += '<div class="radio col-md-2 col-md-offset-1"><label><input type="radio" name="tipo-proprietario' + index + '" id="tipo-proprietario-fisica" value="1">Pessoa Física</label></div>';
            codigoHtmlProprietario += '<div class="radio col-md-2"><label><input type="radio" name="tipo-proprietario' + index + '" id="tipo-proprietario-juridica" value="2">Pessoa Jurídica</label></div></div>';
            var htmlProprietario = new html('#' + idDivProprietarios);
            htmlProprietario.append(codigoHtmlProprietario);

            var htmlNome = new html('#' + idDivProprietarios, 'Nome', this.nome, 'nome-proprietario' + index, 'nome-proprietario' + index, null);
            htmlNome.addInputtext();

            var htmlRg = new html('#' + idDivProprietarios, 'RG', this.rg, 'rg-proprietario' + index, 'rg-proprietario' + index, null);
            htmlRg.addInputtext();

            var htmlCpf = new html('#' + idDivProprietarios, 'CPF', this.cpf, 'cpf-proprietario-pf' + index, 'cpf-proprietario-pf' + index, null);
            htmlCpf.addInputtext();

            var htmlCnpj = new html('#' + idDivProprietarios, 'CNPJ', this.cnpj, 'cnpj-proprietario-pj' + index, 'cnpj-proprietario-pj' + index, null);
            htmlCnpj.addInputtext();

            var htmlEndereco = new html('#' + idDivProprietarios, 'Endereço', this.endereco, 'endereco-proprietario' + index, 'endereco-proprietario' + index, null);
            htmlEndereco.addInputtext();

            var htmlNacionalidade = new html('#' + idDivProprietarios, 'Nacionalidade', this.nacionalidade, 'nacionalidade-proprietario' + index, 'nacionalidade-proprietario' + index, null);
            htmlNacionalidade.addInputtext();

            var htmlProfissao = new html('#' + idDivProprietarios, 'Profissão', this.profissao, 'profissao-proprietario' + index, 'profissao-proprietario' + index, null);
            htmlProfissao.addInputtext();

        } catch (ex) {
            console.log("Erro ao adicionar proprietario " + ex);
        }
    }

    this.addContatoProposta = function (index) {
        try {
            var idDivContatos = 'contato' + index;
            var htmlContatos = new html('#divContatos', '', '', '', '', '');
            htmlContatos.append('<h5>Contato ' + index + '</h5><div id="' + idDivContatos + '"></div>');

            var htmlNome = new html('#' + idDivContatos, 'Nome', this.nome, 'nome-contato' + index, 'nome-contato' + index, null);
            htmlNome.addInputtext();

            var htmlTel1 = new html('#' + idDivContatos, 'Telefone 1', this.telefone, 'telefone-contato' + index, 'telefone-contato' + index, null);
            htmlTel1.addInputtext();

            var htmlTel2 = new html('#' + idDivContatos, 'Telefone 2', this.telefone2, 'telefone2-contato' + index, 'telefone2-contato' + index, null);
            htmlTel2.addInputtext();

            var htmlEmail = new html('#' + idDivContatos, 'E-Mail', this.email, 'email-contato' + index, 'email-contato' + index, null);
            htmlEmail.addInputtext();

        } catch (ex) {
            console.log("Erro ao adicionar proprietario " + ex);
        }
    }

}