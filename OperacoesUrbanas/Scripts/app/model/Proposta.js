﻿var id = null;

function Proposta(calculoOpUrbana) {

    this.calculoOpUrbana = calculoOpUrbana ? calculoOpUrbana : null;

    this.imoveis = [];
    
    this.interessados = [];

    this.proprietarios = [];

    this.contatos = [];

    this.addImovel = function () {
        this.atualizarImoveis();
        var novoImovel = new imovel();
        this.imoveis.push(novoImovel);
        var index = this.imoveis.length;
        novoImovel.addImovel(index);
        this.atualizarHtmlImoveis();
    }

    this.atualizarImoveis = function () {
        if (this.imoveis != null && this.imoveis.length > 0) {
            for (var i = 0; i < this.imoveis.length; i++) {
                var imovel = this.imoveis[i];
                var indice = i + 1;
                imovel.contribuinte = $('#codContribuinte' + indice).val();
                imovel.areaReal = new html('#areaReal' + indice).getElementFloatVal(2);
                imovel.areaEscritura = new html('#areaEscritura' + indice).getElementFloatVal(2);
            }
        }
    }

    this.atualizarImoveisCompl = function () {
        if (this.imoveis != null && this.imoveis.length > 0) {
            for (var i = 0; i < this.imoveis.length; i++) {
                var imovel = this.imoveis[i];
                var indice = i + 1;
                imovel.id = $('#idCompl' + indice).val();
                imovel.contribuinte = $('#codContribuinteCompl' + indice).val();
                imovel.areaReal = new html('#areaRealCompl' + indice).getElementFloatVal(2);
                imovel.areaEscritura = new html('#areaEscrituraCompl' + indice).getElementFloatVal(2);
                imovel.matricula = new html('#matriculaCompl' + indice).getElementFloatVal(2);
                imovel.endereco = $('#enderecoCompl' + indice).val();
            }
        }
    }

    /*
    O HTML dos imóveis anteriores estão fixados com os valores iniciais,
    A partir disso, quando adiciono um novo imóvel perco os valores do vetor.
    Necessário recuperar os valores do vetor para o HTML sempre que adicionar um novo imóvel.
    */
    this.atualizarHtmlImoveis = function () {
        if (this.imoveis != null && this.imoveis.length > 0) {
            for (var i = 0; i < this.imoveis.length; i++) {
                var imovel = this.imoveis[i];
                var indice = i + 1;
                $('#codContribuinte' + indice).val(imovel.contribuinte);
                $('#areaReal' + indice).val(base.methods.floatToCurrency(imovel.areaReal));
                $('#areaEscritura' + indice).val(base.methods.floatToCurrency(imovel.areaEscritura));
            }
        }
    }

    this.atualizarArea = function () {
        this.atualizarImoveis();
        if (this.imoveis != null && this.imoveis.length > 0) {
            var somaReal = 0;
            var somaEscritura = 0;
            for (var i = 0; i < this.imoveis.length; i++) {
                var imovel = this.imoveis[i];
                somaReal += parseFloat(imovel.areaReal);
                somaEscritura += parseFloat(imovel.areaEscritura);
            }
            $('#areaRealTotal').val(base.methods.floatToCurrency(somaReal));
            $('#areaEscrituraTotal').val(base.methods.floatToCurrency(somaEscritura));

            var ato = (somaReal < somaEscritura) ? somaReal : somaEscritura;
            var atdmv = 0;
            var atd = 0;
            var atdd = 0;
            var atsd = 0;

            var desapropriacao = $('input[name=pedidoAlvara]:checked');
            /*
            if (desapropriacao && desapropriacao.val() == '0') {
                ato = $('#ato-original').val() != '' ? parseFloat($('#ato-original').val()) : 0;
            }else
            */
            if (desapropriacao && desapropriacao.val() == '1') {
                atdmv = $('#atdmv').val() != '' ? base.methods.currencyToFloat($('#atdmv').val()) : 0;
                atdd = $('#atdd').val() != '' ? base.methods.currencyToFloat($('#atdd').val()) : 0;
                atd = (atdmv + atdd);
            }

            var doacao = $('input[name=doacaoDesapropriacao]:checked');
            if (doacao && doacao.val() == '0') {
                atsd = ato - atd;
                atsd = Number(Math.round(atsd + 'e2') + 'e-2');
                ato = atsd;
                
            }
            else if (doacao && doacao.val() == '1') {
                ato = $('#ato-acca').val() != '' ? base.methods.currencyToFloat($('#ato-acca').val()) : 0;
            }

            $('#ato').val(base.methods.floatToCurrency(ato));
            $('#atd').val(base.methods.floatToCurrency(atd));
            $('#atsd').val(base.methods.floatToCurrency(atsd));
            if (this.calculoOpUrbana != null) {
                this.calculoOpUrbana.atualizarTela();
            }
        }
    }

    this.addInteressado = function () {
        this.atualizarInteressados();
        var novoInteressado = new Pessoa();
        this.interessados.push(novoInteressado);
        var index = this.interessados.length;
        novoInteressado.addInteressadoCepac(index);
        this.atualizarHtmlInteressados();
    }

    this.atualizarInteressados = function () {
        if (this.interessados != null && this.interessados.length > 0) {
            for (var i = 0; i < this.interessados.length; i++) {
                var interessado = this.interessados[i];
                var indice = i + 1;
                interessado.nome = $('#nome-interessado' + indice).val();
                interessado.endereco = $('#endereco-interessado' + indice).val();
                interessado.rg = $('#rg-interessado' + indice).val();
                interessado.cpf = $('#cpf-interessado-pf' + indice).val();
                interessado.cnpj = $('#cnpj-interessado-pj' + indice).val();
                interessado.nacionalidade = $('#nacionalidade-interessado' + indice).val();
                interessado.profissao = $('#profissao-interessado' + indice).val();
            }
        }
    }

    this.atualizarHtmlInteressados = function () {
        if (this.interessados != null && this.interessados.length > 0) {
            for (var i = 0; i < this.interessados.length; i++) {
                var interessado = this.interessados[i];
                var indice = i + 1;
                 $('#nome-interessado' + indice).val(interessado.nome);
                 $('#endereco-interessado' + indice).val(interessado.endereco);
                 $('#rg-interessado' + indice).val(interessado.rg);
                 $('#cpf-interessado-pf' + indice).val(interessado.cpf);
                 $('#cnpj-interessado-pj' + indice).val(interessado.cnpj);
                 $('#nacionalidade-interessado' + indice).val(interessado.nacionalidade);
                 $('#profissao-interessado' + indice).val(interessado.profissao);
            }
        }
    }

    this.addProprietario = function () {
        this.atualizarProprietarios();
        var novoProprietario = new Pessoa();
        this.proprietarios.push(novoProprietario);
        var index = this.proprietarios.length;
        novoProprietario.addProprietarioCepac(index);
        this.atualizarHtmlProprietario();
    }

    this.atualizarProprietarios = function () {
        if (this.proprietarios != null && this.proprietarios.length > 0) {
            for (var i = 0; i < this.proprietarios.length; i++) {
                var proprietario = this.proprietarios[i];
                var indice = i + 1;
                proprietario.nome = $('#nome-proprietario' + indice).val();
                proprietario.endereco = $('#endereco-proprietario' + indice).val();
                proprietario.rg = $('#rg-proprietario' + indice).val();
                proprietario.cpf = $('#cpf-proprietario-pf' + indice).val();
                proprietario.cnpj = $('#cnpj-proprietario-pj' + indice).val();
                proprietario.nacionalidade = $('#nacionalidade-proprietario' + indice).val();
                proprietario.profissao = $('#profissao-proprietario' + indice).val();
            }
        }
    }

    this.atualizarHtmlProprietario = function () {
        if (this.proprietarios != null && this.proprietarios.length > 0) {
            for (var i = 0; i < this.proprietarios.length; i++) {
                var proprietario = this.proprietarios[i];
                var indice = i + 1;
                $('#nome-proprietario' + indice).val(proprietario.nome);
                $('#endereco-proprietario' + indice).val(proprietario.endereco);
                $('#rg-proprietario' + indice).val(proprietario.rg);
                $('#cpf-proprietario-pf' + indice).val(proprietario.cpf);
                $('#cnpj-proprietario-pj' + indice).val(proprietario.cnpj);
                $('#nacionalidade-proprietario' + indice).val(proprietario.nacionalidade);
                $('#profissao-proprietario' + indice).val(proprietario.profissao);
            }
        }
    }

    this.addContato = function () {
        this.atualizarContatos();
        var novoContato = new Pessoa();
        this.contatos.push(novoContato);
        var index = this.contatos.length;
        novoContato.addContatoProposta(index);
        this.atualizarHtmlContatos();
    }

    this.atualizarContatos = function () {
        if (this.contatos != null && this.contatos.length > 0) {
            for (var i = 0; i < this.contatos.length; i++) {
                var contato = this.contatos[i];
                var indice = i + 1;
                contato.nome = $('#nome-contato' + indice).val();
                contato.telefone = $('#telefone-contato' + indice).val();
                contato.telefone2 = $('#telefone2-contato' + indice).val();
                contato.email = $('#email-contato' + indice).val();
            }
        }
    }

    this.atualizarHtmlContatos = function () {
        if (this.contatos != null && this.contatos.length > 0) {
            for (var i = 0; i < this.contatos.length; i++) {
                var contato = this.contatos[i];
                var indice = i + 1;
                 $('#nome-contato' + indice).val(contato.nome);
                 $('#telefone-contato' + indice).val(contato.telefone);
                 $('#telefone2-contato' + indice).val(contato.telefone2);
                 $('#email-contato' + indice).val(contato.email);
            }
        }
    }

    this.atualizarCalculo = function () {
        if (this.calculoOpUrbana != null) {
            calculoOpUrbana.atualizarTela();
        }
    }

    this.finalizar = function () {
        if (this.calculoOpUrbana != null) {
            var proposta = {};
            //proposta.idTipoCertidao = $('input[name=tipoCertidao]:checked').val();
            proposta.idTipoCertidao = '1';
            proposta.idTipoPedido = '2';
            proposta.idSubSetor = idSubSetor;
            proposta.idZonaAtual = $('#zonaAtual').val();
            proposta.idSubCategoriaUso = $('#subCategoriaUso').val();
            //proposta._acca = $('#acca') ? $('#acca:checked').val() : '0';
            proposta.taxaOcupacaoMaxima = $('#taxaOcupacaoMaxima').val();
            proposta.gabarito = $('#gabarito').val();
            proposta.caBasico = $('#caBasico').val();
            proposta.caProjeto = calculoOpUrbana.caProjeto();
            proposta.areaRealTotal = $('#areaRealTotal').val();
            proposta.areaEscrituraTotal = $('#areaEscrituraTotal').val();
            proposta.qtdeCepac = calculoOpUrbana.qt();
            proposta.imoveis = this.imoveis;
            this.salvar(proposta);
        }
    }


    this.simular = function () {
        var idFormSimulacao = '#' + calculoOpUrbana.getIdFormSimulacao();
        if ($(idFormSimulacao)) {
            $(idFormSimulacao).submit();
        }
    }


    this.salvar = function (proposta) {
        var url = apiUrl + 'propostas/finalizar';

        //Da maneira simplificada $.post(...) nao passa a lista de objetos dentro do objeto
        $.ajax({
            type: "POST",
            data: JSON.stringify(proposta),
            url: url,
            contentType: "application/json"
        }).then(function (propostaFinalizada) {
            id = propostaFinalizada.id;

            calculoOpUrbana.criarValores(id);
            var imoveisZerados = 0;
            for (var i = 0; i < propostaFinalizada.imoveis.length; i++) {
                var imovelProposta = propostaFinalizada.imoveis[i];
                if (imovelProposta.areaReal > 0 || imovelProposta.areaEscritura > 0) {
                    addFormComplementoImovel(imovelProposta, (i - imoveisZerados));
                } else {
                    imoveisZerados++;
                }


            }
            $('#botoes-conclusao').hide();
            $('#botoes-encerramento').show();
            $('.segundo-passo').show();
            $('.primeiro-passo').hide();
            $('#dlgValidar').modal('toggle');
            base.methods.showMessage('Proposta salva! Favor complementar as informações na tela a seguir.');

            //adiciona automaticamente 1 interessado
            $('#btnAddInteressado').click();

        }).fail(function (resp) {
            base.methods.showMessage('Erro ao finalizar proposta! ' + resp.status + ' ' + resp.statusText);
        });

    }


    this.encerrar = function () {
        if (this.calculoOpUrbana != null) {

            this.atualizarImoveisCompl();
            this.atualizarInteressados();
            this.atualizarProprietarios();
            this.atualizarContatos();

            var proposta = {};
            //proposta.idTipoCertidao = $('input[name=tipoCertidao]:checked').val();
            proposta.id = id;
            proposta.idTipoCertidao = '1';
            proposta.idTipoPedido = '2';
            proposta.idSubSetor = idSubSetor;
            proposta.idZonaAtual = $('#zonaAtual').val();
            proposta.idSubCategoriaUso = $('#subCategoriaUso').val();
            proposta._acca = $('#acca:checked') ? $('#acca:checked').val() : '0';
            proposta.taxaOcupacaoMaxima = $('#taxaOcupacaoMaxima').val();
            proposta.gabarito = $('#gabarito').val();
            proposta.caBasico = $('#caBasico').val();
            proposta.caProjeto = calculoOpUrbana.caProjeto();
            proposta.areaRealTotal = base.methods.currencyToFloat($('#areaRealTotal').val());
            proposta.areaEscrituraTotal = base.methods.currencyToFloat($('#areaEscrituraTotal').val());
            proposta.qtdeCepac = calculoOpUrbana.qt();
            proposta.imoveis = this.imoveis;
            proposta.interessados = this.interessados;
            proposta.proprietarios = this.proprietarios;
            proposta.contatos = this.contatos;


            var url = apiUrl + 'propostas/encerrar';

            //Da maneira simplificada $.post(...) nao passa a lista de objetos dentro do objeto
            $.ajax({
                type: "POST",
                data: JSON.stringify(proposta),
                url: url,
                contentType: "application/json"
            }).then(function (propostaFinalizada) {
                $('#dlgEncerrar').modal('toggle');
                base.methods.showMessage('Proposta ID '+ propostaFinalizada.id + ' enviada com sucesso! ');
            }).fail(function (resp) {
                base.methods.showMessage('Erro ao encerrar proposta! ' + resp.status + ' ' + resp.statusText);
            });

        }

    }

    this.getImoveisProposta = function (idProposta) {
        var url = apiUrl + 'imoveis/proposta/' + id;
        var imoveis = [];
        $.post(url)
            .then(function (resp) {
                for (var i = 0; i < resp.length; i++) {
                    var imovelProposta = resp[i];
                    imoveis.push(imovelProposta.imovel);
                }
            })
            .fail(function (resp) {
                alert('Erro ao obter imoveis proposta! ' + resp.status + ' ' + resp.statusText);
            });

    }

    this.cancelar = function (idProposta) {
        var url = apiUrl + 'propostas/remover';
        var postdata = { id: idProposta };
        $.post(url, postdata)
            .then(function (resp) {
                alert('Proposta ' + idProposta + ' cancelada!');
            })
            .fail(function (resp) {
                alert('Erro ao cancelar proposta! ' + resp.status + ' ' + resp.statusText);
            });
    }

    this.imprimirFormRosto = function () {
        
        this.atualizarInteressados();
        this.atualizarContatos();
        this.atualizarProprietarios();

        calculoOpUrbana.atualizarTela();

        $('input[name=contatos]').val(JSON.stringify(this.contatos));
        $('input[name=interessados]').val(JSON.stringify(this.interessados));
        $('input[name=proprietarios]').val(JSON.stringify(this.proprietarios));

        $('input[name=acca]').val($('#acca').is(':checked') ? 'SIM' : 'NÃO');
        $('input[name=mupu]').val($('#mupu').is(':checked') ? 'SIM' : 'NÃO');
        $('input[name=aumentoTaxaOcupacao]').val($('#aumentoTaxaOcupacao').is(':checked') ? 'SIM' : 'NÃO');

        $('#frm-rosto').submit();
    }

}