﻿var idElOpUrbana = '#operacaoUrbana';
var idElResultado = '#resultado';
var idElParametro = '#parametro';
var idElFormula = '#formula-montada';

var parametros = [];
var ordem = 0;

$(document).ready(function () {

    loadOperacoesUrbanas();

    $('#btnAddParametro').click(function () {
        if ($(idElParametro).val() != '') {
            addParametro();
        }
    });

    $(idElOpUrbana).change(function () {
        ordem = 0;
        var idOpUrbana = $(idElOpUrbana).val();
        $(idElResultado).html('');
        $('#formula-resultado').html('');
        $('#msg-status').html('');
        $(idElFormula).html('');
        loadParametrosResultado(idOpUrbana);
    });

    $(idElResultado).change(function () {
        ordem = 0;
        var idOpUrbana = $(idElOpUrbana).val();
        var idResultado = $(idElResultado).val();
        var txtResultado = $(idElResultado + ' option:selected').text();
        $('#formula-resultado').html(txtResultado.split(' - ')[0] + ' = ');
        $('#msg-status').html('');
        loadParametros();
    });

    $('#btnSave').click(function () {
        salvarFormula();
    });

    $('#btnReset').click(function () {
        reset();
    });

})


function parametroOpUrbana(idOperacaoUrbana, idParametro, idParametroPai, ordem, sigla) {
    this.idOperacaoUrbana = idOperacaoUrbana;
    this.idParametro = idParametro;
    this.idParametroPai = idParametroPai;
    this.ordem = ordem;
    this.sigla = sigla;
}

function addParametro() {
    var idParametroSelecionado = $(idElParametro).val();
    var siglaParametroSelecionado = $(idElParametro + ' option:selected').text();
    var idParametroPaiSelecionado = $(idElResultado).val();
    var idOperacaoUrbanaSelecionada = $(idElOpUrbana).val();
    var novoParametro = new parametroOpUrbana(idOperacaoUrbanaSelecionada, idParametroSelecionado, idParametroPaiSelecionado, ++ordem, siglaParametroSelecionado);
    parametros.push(novoParametro);
    loadFormulaMontada();
}

function loadFormulaMontada() {
    var formula = '';
    for (var i = 0; i < parametros.length; i++) {
        var parametro = parametros[i];
        formula = formula + parametro.sigla.split(" - ")[0];
    }
    $(idElFormula).html(formula);
}

function loadOperacoesUrbanas() {
    var htmlOperacoes = new html(idElOpUrbana, '', '', 'operacaoUrbana', 'operacaoUrbana', 0);
    base.methods.getAll(htmlOperacoes, 'OpUrbanas');
}

function loadParametrosResultado(idOpUrbana) {
    $(idElResultado).html('<option value="">- Selecionar -</option>');
    var htmlResultados = new html(idElResultado, '', '', 'resultado', 'resultado', 0);
    getAllParametros(htmlResultados, 'parametrosoperacaourbana/var/' + idOpUrbana);
}

function loadParametros() {
    $(idElParametro).html('<option value="">- Selecionar -</option>');
    var htmlParametros = new html(idElParametro, '', '', 'parametro', 'parametro', 0);
    getAllParametros(htmlParametros, 'parametros');
}

function getAllParametros(htmlObject, apiMethod) {
    var htmlElement = htmlObject;
    var url = apiUrl + apiMethod;
    $.get(url)
        .then(function (resp) {
            if (resp == null || resp == "" || resp.length < 1) {
                console.log("Resposta sem dados " + url);
            } else {
                for (var i = 0; i < resp.length; i++) {
                    var e = resp[i];
                    if(e.parametro){
                        htmlElement.text = e.parametro != null ? e.parametro.sigla + ' - ' + e.parametro.descricao : '';
                    }else if(e.sigla){
                        htmlElement.text = e.sigla != null ? e.sigla + ' - ' + e.descricao : '';
                    }
                    htmlElement.value = e.id;
                    htmlElement.id = e.id;
                    (htmlElement.index != null) ? htmlElement.addSelectOption() : htmlElement.addBootstrapRadioOption();
                }
            }
        })
        .fail(function (resp) {
            console.log("Erro ao obter resposta " + url);
            console.log(resp.responseText);
        });
}

function salvarFormula() {
    if (parametros.length > 1) {
        try {
            for (var i = 0; i < parametros.length; i++) {
                salvarParametro(parametros[i]);
            }
        } catch (ex) {
            alert(ex);
        }
    } else {
        alert('Favor completar a fórmula.');
    }
}

function salvarParametro(parametro) {
    if (parametro) {
        var url = apiUrl + 'ParametrosOperacaoUrbana';
        var data = parametro;

        $.post(url, data)
            .then(function (resp) {
                $('#msg-status').html('Cadastrado com sucesso!')
            })
            .fail(function (resp) {
                $('#msg-status').html('');
                var msg = '';
                switch (resp.status) {
                    case 400: msg = 'Registro já cadastrado.'; break;
                    case 403: msg = 'Sem permissão para completar o cadastro.'; break;
                    default: msg = 'Não foi possível concluir a operação. Se o problema persistir, acione o suporte técnico.'; break;
                }
                throw msg;
            });
    }
}

function reset() {
    var fields = [idElResultado, idElParametro, idElFormula];
    for (var i = 0; i < fields.length; i++) {
        $(fields[i]).html('');
    }
    $(idElParametro + ' option:selected').val('');
    parametros.length = 0;
}