﻿function CalculoAguaEspraiada() {

    this.ato = function () {
        return new html('#ato').getElementFloatVal(2);
    }
    this.caBasico = function () {
        return new html('#caBasico').getElementFloatVal(4);
    }
    this.caProjeto = function () {
        return new html('#ae-caProjeto').getElementFloatVal(4);
    }
    this.armb = function () {
        return new html('#ae-armb').getElementFloatVal(2);
    }
    this.atdc = function () {
        var atdc1 = new html('#ae-atdc').getElementFloatVal(2);
        if (atdc1 > 0) {
            return atdc1;
        }
        return new html('#ae-atdc-2').getElementFloatVal(2);
    }

    this.f1 = function () {
        return new html('#ae-f1').getElementFloatVal(2);
    }

    this.f2 = function () {
        return new html('#ae-f2').getElementFloatVal(2);
    }

    this.atdmv = function () {
        var display = $('input[name=pedidoAlvara]:checked');
        if (!display || display.val() == '0') {
            return 0;
        }
        var atdmv = $('#atdmv').val();
        if (atdmv != '') {
            return new html('#atdmv').getElementFloatVal(2);
        } else {
            return new html('#ae-atdmv').getElementFloatVal(2);
        }
    }

    this.aca = function () {
        var val = (this.ato() * this.caProjeto()) - (this.ato() * this.caBasico());
        return !isNaN(val) ? parseFloat(val.toFixed(2)) : 0;
    }

    this.i1 = function () {
        var val = 0;
        if (this.armb() >= 5000) {
            val = (20 / 100 * this.armb());
        } else if (this.armb() >= 2500) {
            val = (10 / 100 * this.armb());
        }
        return !isNaN(val) ? parseFloat(val.toFixed(2)) : 0;
    }

    this.i2 = function () {
        var val = this.atdc() * 2;
        return !isNaN(val) ? parseFloat(val.toFixed(2)) : 0;
    }

    this.qad = function () {
        var display = $('input[name=aumentoAproveitamento]:checked');
        if (!display || display.val() == '0') {
            return 0;
        }
        var val = (this.aca() - (this.i1() + this.i2()) / this.f1());
        return !isNaN(val) ? Math.ceil(val) : 0;
    }

    this.atrmp = function () {
        var val = this.ato() - (this.atdc() + this.atdmv());
        return !isNaN(val) ? parseFloat(val.toFixed(2)) : 0;
    }

    this.qmp = function () {
        var display = $('input[name=modificacaoUso]:checked');
        if (!display || display.val() == '0') {
            return 0;
        }
        var val = (this.atrmp() / this.f2());
        return !isNaN(val) ? Math.ceil(val) : 0;
    }

    this.qt = function () {
        var val = (this.qad() + this.qmp());
        return !isNaN(val) ? Math.ceil(val) : 0;
    }

    this.atualizarTela = function () {
        $('input[name=ato]').val(base.methods.floatToCurrency(this.ato()));
        $('#ato-original').val(base.methods.floatToCurrency(this.ato()));
        $('input[name=caBasico]').val(this.caBasico());
        $('input[name=aca]').val(base.methods.floatToCurrency(this.aca()));
        $('input[name=i1]').val(base.methods.floatToCurrency(this.i1()));
        $('input[name=i2]').val(base.methods.floatToCurrency(this.i2()));
        $('input[name=qad]').val(this.qad());
        $('input[name=f1]').val(this.f1());
        $('input[name=f2]').val(this.f2());
        $('input[name=atdc]').val(base.methods.floatToCurrency(this.atdc()));
        $('#ae-atdc-2').val(base.methods.floatToCurrency(this.atdc()));
        $('input[name=atdmv]').val(base.methods.floatToCurrency(this.atdmv()));
        $('input[name=atrmp]').val(base.methods.floatToCurrency(this.atrmp()));
        $('input[name=qmp]').val(this.qmp());
        $('input[name=qt]').val(this.qt());
    }


    this.gerarValoresParametros = function (idProposta) {
        var nomes = ['ATO', 'CABASICO', 'CAPROJETO', 'ARMB', 'ATDC', 'ATDMV', 'ACA', 'F1', 'F2', 'I1', 'I2', 'QAD', 'ATRMP', 'QMP', 'QT'];
        var variaveis = [this.ato(), this.caBasico(), this.caProjeto(), this.armb(), this.atdc(), this.atdmv(), this.aca(), this.f1(), this.f2(), this.i1(), this.i2(), this.qad(), this.atrmp(), this.qmp(), this.qt()];
        var valoresParametros = [];
        for (var i = 0; i < nomes.length; i++) {
            var obj = { idProposta: idProposta, nomeParametro: nomes[i], valor: variaveis[i] };
            valoresParametros.push(obj);
        }
        return valoresParametros;
    }


    this.criarValores = function (idProposta) {
        gravarValores(this.gerarValoresParametros(idProposta), 0);
    }

    this.carregarVariaveisFixas = function (idSubSetor, idCategoria) {
        if (idSubSetor && idSubSetor != null && idSubSetor > 0) {
            this.setValorVariavelFixa('ae-f1', 'F1', idSubSetor, null);
            this.setValorVariavelFixa('ae-f2', 'F2', idSubSetor, null);
        }
    }

    this.setValorVariavelFixa = function (idElemento, nomeVariavel, idSubSetor, idCategoriaUso) {
        var url = apiUrl.concat('variaveis/', nomeVariavel, '/', idSubSetor);
        $.get(url).then(function (data) {
            $('#'+idElemento).val(data.valor);
        });
    }

    this.getIdFormSimulacao = function () {
        return 'frm-simulacao-ae';
    }

    //Init
    this.atualizarTela();

}

$(function () {
    $('#aumentoTaxaOcupacao').hide();

    $('input[name=aumentoAproveitamento]').change(function () {
        if ($(this).val() == '1') {
            $('#ae-calculo-qad').show();
        } else {
            $('#ae-calculo-qad').hide();
        }
    });

    $('input[name=modificacaoUso]').change(function () {
        if ($(this).val() == '1') {
            $('#ae-calculo-qmp').show();
        } else {
            $('#ae-calculo-qmp').hide();
        }
    });
});