﻿var idCategoriaMisto = '3';

function CalculoFariaLima() {

    this.ato = function () {
        return new html('#ato').getElementFloatVal(2);
    }
    this.caBasico = function () {
        return new html('#caBasico').getElementFloatVal(4);
    }
    this.caProjeto = function () {
        return new html('#fl-caProjeto').getElementFloatVal(4);
    }
    this.apr = function () {
        return new html('#fl-apr').getElementFloatVal(2);
    }
    this.apnr = function () {
        var val = 100 - this.apr();
        return !isNaN(val) ? parseFloat(val.toFixed(2)) : 0;
    }
    this.armb = function () {
        return new html('#fl-armb').getElementFloatVal(2);
    }
    this.atd = function () {
        return new html('#fl-atd').getElementFloatVal(2);
    }
    this.f1 = function () {
        return new html('#fl-f1').getElementFloatVal(2);
    }
    this.f1r = function () {
        return new html('#fl-f1r').getElementFloatVal(2);
    }
    this.f1nr = function () {
        return new html('#fl-f1nr').getElementFloatVal(2);
    }
    this.f2 = function () {
        return new html('#fl-f2').getElementFloatVal(2);
    }
    this.f3 = function () {
        return new html('#fl-f3').getElementFloatVal(2);
    }
    this.f3r = function () {
        return new html('#fl-f3r').getElementFloatVal(2);
    }
    this.f3nr = function () {
        return new html('#fl-f3nr').getElementFloatVal(2);
    }
    this.ape = function () {
        return new html('#fl-ape').getElementFloatVal(2);
    }

    this.aca = function () {
        var val = (this.ato() * this.caProjeto()) - (this.ato() * this.caBasico());
        return !isNaN(val) ? parseFloat(val.toFixed(2)) : 0;
    }

    this.acar = function () {
        var val = 0;
        if (this.apr() > 0) {
            val = (((this.apr() / 100) * this.ato()) * this.caProjeto()) - (((this.apr() / 100) * this.ato()) * this.caBasico());
        }
        return !isNaN(val) ? parseFloat(val.toFixed(2)) : 0;
    }

    this.acanr = function () {
        var val = 0;
        if (this.apnr() > 0) {
            val = (((this.apnr() / 100) * this.ato()) * this.caProjeto()) - (((this.apnr() / 100) * this.ato()) * this.caBasico());
        }
        return !isNaN(val) ? parseFloat(val.toFixed(2)) : 0;
    }

    this.i1 = function () {
        var val = 0;
        if (this.armb() >= 2500) {
            val = (20 / 100 * this.armb()); 
        }
        return !isNaN(val) ? parseFloat(val.toFixed(2)) : 0;
    }

    this.i1r = function () {
        var val = 0;
        if (this.apr() > 0) {
            val = ((this.apr() / 100) * this.armb()) * this.i1();
        }
        return !isNaN(val) ? parseFloat(val.toFixed(2)) : 0;
    }

    this.i1nr = function () {
        var val = 0;
        if (this.apnr() > 0) {
            val = ((this.apnr() / 100) * this.armb()) * this.i1();
        }
        return !isNaN(val) ? parseFloat(val.toFixed(2)) : 0;
    }

    this.i2 = function () {
        var val = 0;
        if (this.ato() >= 2000) {
            var usoPublico = $('input[name=usoPublico]:checked');
            if (usoPublico && usoPublico.val() == '1') {
                val = (20 / 100 * this.ato());
            }
        }
        return !isNaN(val) ? parseFloat(val.toFixed(2)) : 0;
    }

    this.i2r = function () {
        var val = 0;
        if (this.apr() > 0) {
            val = ((this.apr() / 100) * this.ato()) * this.i1();
        }
        return val;
    }

    this.i2nr = function () {
        var val = 0;
        if (this.apnr() > 0) {
            val = ((this.apnr() / 100) * this.ato()) * this.i1();
        }
        return val;
    }

    this.qad = function () {
        var display = $('input[name=aumentoAproveitamento]:checked');
        if (!display || display.val() == '0') {
            return 0;
        }
        var val = 0;
        if ($('#categoriaUso').val() == idCategoriaMisto) {
            val = this.qadr() + this.qadnr();
        } else {
            val = (this.aca() - this.i1() - this.i2()) / this.f1();
        }
        return !isNaN(val) ? Math.ceil(val) : 0;
    }

    this.qadr = function () {
        var val = 0;
        val = (this.acar() - this.i1r() - this.i2r()) / this.f1r();
        return !isNaN(val) ? Math.ceil(val) : 0;
    }

    this.qadnr = function () {
        var val = 0;
        val = (this.acanr() - this.i1nr() - this.i2nr()) / this.f1nr();
        return !isNaN(val) ? Math.ceil(val) : 0;
    }

    this.aape = function () {
        var val = this.ape() - (50/100 * this.ato());
        return !isNaN(val) ? parseFloat(val.toFixed(2)) : 0;
    }

    this.qto = function () {
        var display = $('input[name=aumentoTaxaOcupacao]:checked');
        if (!display || display.val() == '0') {
            return 0;
        }
        var val = this.aape() / this.f3();
        return !isNaN(val) ? Math.ceil(val) : 0;
    }

    this.qmp = function () {
        var display = $('input[name=modificacaoUso]:checked');
        if (!display || display.val() == '0') {
            return 0;
        }
        var val = (this.ato() - this.atd()) / this.f2();
        return !isNaN(val) ? Math.ceil(val) : 0;
    }

    this.qt = function () {
        var val = (this.qad() + this.qmp() + this.qto());
        return !isNaN(val) ? Math.ceil(val) : 0;
    }

    this.atualizarTela = function () {
        $('input[name=ato]').val(base.methods.floatToCurrency(this.ato()));
        $('#ato-original').val(base.methods.floatToCurrency(this.ato()));
        $('input[name=caBasico]').val(this.caBasico());
        $('input[name=apr]').val(base.methods.floatToCurrency(this.apr()));
        $('input[name=apnr]').val(base.methods.floatToCurrency(this.apnr()));
        $('input[name=aca]').val(base.methods.floatToCurrency(this.aca()));
        $('input[name=i1]').val(base.methods.floatToCurrency((this.i1())));
        $('input[name=i2]').val(base.methods.floatToCurrency(this.i2()));
        $('input[name=qad]').val(this.qad());
        $('input[name=f1]').val(this.f1());
        $('#fl-f1r').val(this.f1r());
        $('#fl-f1nr').val(this.f1nr());
        $('input[name=f2]').val(this.f2());
        $('input[name=f3]').val(this.f3());
        $('#fl-f3r').val(this.f3r());
        $('#fl-f3nr').val(this.f3nr());
        $('input[name=atd]').val(base.methods.floatToCurrency(this.atd()));
        $('input[name=qmp]').val(this.qmp());
        $('input[name=ape]').val(base.methods.floatToCurrency(this.ape()));
        $('input[name=aape]').val(base.methods.floatToCurrency(this.aape()));
        $('input[name=qto]').val(this.qto());
        $('input[name=qt]').val(this.qt());
    }

    this.gerarValoresParametros = function (idProposta) {
        var nomes = ['ATO', 'CABASICO', 'CAPROJETO', 'ARMB', 'ACA', 'APE', 'AAPE', 'F1', 'F2', 'F3', 'I1', 'I2', 'QAD', 'QMP', 'QTO', 'QT'];
        var variaveis = [this.ato(), this.caBasico(), this.caProjeto(), this.armb(), this.aca(), this.ape(), this.aape(), this.f1(), this.f2(), this.f3(), this.i1(), this.i2(), this.qad(), this.qmp(), this.qto(), this.qt()];
        var valoresParametros = [];
        for (var i = 0; i < nomes.length; i++) {
            var obj = { idProposta: idProposta, nomeParametro: nomes[i], valor: variaveis[i] };
            valoresParametros.push(obj);
        }
        return valoresParametros;
    }

    this.criarValores = function (idProposta) {
        gravarValores(this.gerarValoresParametros(idProposta), 0);
    }

    this.carregarVariaveisFixas = function (idSubSetor, idCategoria) {
        if (idSubSetor && idCategoria) {
            if (idCategoria == idCategoriaMisto) {
                this.setValorVariavelFixa('fl-f2', 'F2', idSubSetor, idCategoria);
                this.setValorVariavelFixa('fl-f1r', 'F1', idSubSetor, 1);
                this.setValorVariavelFixa('fl-f1nr', 'F1', idSubSetor, 2);
                this.setValorVariavelFixa('fl-f3r', 'F3', idSubSetor, 1);
                this.setValorVariavelFixa('fl-f3nr', 'F3', idSubSetor, 2);
            } else {
                this.setValorVariavelFixa('fl-f1', 'F1', idSubSetor, idCategoria);
                this.setValorVariavelFixa('fl-f2', 'F2', idSubSetor, idCategoria);
                this.setValorVariavelFixa('fl-f3', 'F3', idSubSetor, idCategoria);
            }
        }
    }

    this.setValorVariavelFixa = function (idElemento, nomeVariavel, idSubSetor, idCategoriaUso) {
        var url = apiUrl.concat('variaveis/', nomeVariavel, '/', idSubSetor, '/', idCategoriaUso);
        $.get(url).then(function (data) {
            $('#' + idElemento).val(data.valor);
        });
    }

    this.getIdFormSimulacao = function () {
        return 'frm-simulacao-fl';
    }

    //Init
    this.atualizarTela();

}

$(function () {
    $('#aumentoTaxaOcupacao').show();

    $('input[name=aumentoAproveitamento]').change(function () {
        if ($(this).val() == '1') {
            $('#fl-calculo-qad').show();
        } else {
            $('#fl-calculo-qad').hide();
        }
    });

    $('input[name=modificacaoUso]').change(function () {
        if ($(this).val() == '1') {
            $('#fl-calculo-qmp').show();
        } else {
            $('#fl-calculo-qmp').hide();
        }
    });

    $('input[name=aumentoTaxaOcupacao]').change(function () {
        if ($(this).val() == '1') {
            $('#fl-calculo-qto').show();
        } else {
            $('#fl-calculo-qto').hide();
        }
    });

    $('#categoriaUso').change(function () {
        var idCategoria = $('#categoriaUso').val();
        if (idCategoria && idCategoria == idCategoriaMisto) {
            $('#fl-area-proporcional').show();
        } else {
            $('#fl-area-proporcional').hide();
        }
    });

});