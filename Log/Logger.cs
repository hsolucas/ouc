﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Log
{
    public class Logger
    {

        private static string LOG_FILE_NAME = "C:\\logs\\sisouc_log.txt";

        private static StreamWriter LOG_WRITER;
        private static StreamReader LOG_READER;

        public static void Write(string logMessage)
        {

            try
            {
                if (!File.Exists(LOG_FILE_NAME))
                {
                    File.Create(LOG_FILE_NAME);
                }

                LOG_WRITER = File.AppendText(LOG_FILE_NAME);
                LOG_WRITER.Write("\r\n");
                LOG_WRITER.WriteLine("[{0} {1}] : ", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
                LOG_WRITER.WriteLine("  {0}", logMessage);
                LOG_WRITER.WriteLine("--------------------------------------");
                LOG_WRITER.Close();
            }
            catch (Exception ex)
            {
                System.Console.Write("Erro ao gravar o log : " + LOG_FILE_NAME + " \n " + ex.ToString());
            }

        }

        public static void Write(Exception e)
        {
            if (e.InnerException != null)
            {
                Write(e.InnerException);
            }
            Write(e.Message + "\r\n" + e.StackTrace);
        }

        public static void DumpLog()
        {
            LOG_READER = File.OpenText(LOG_FILE_NAME);
            string line;
            while ((line = LOG_READER.ReadLine()) != null)
            {
                Console.WriteLine(line);
            }
            LOG_READER.Close();
        }

        public static string GetLogFileName()
        {
            return LOG_FILE_NAME;
        }

    }
}
